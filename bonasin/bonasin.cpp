#include <iostream>
#include <GL/glut.h>
#include <stdio.h>
#include <vector>


char p = 0;
float x = 0.0f, y = 0.0f;
float radius = 0.5;

struct Cell
{
	float t, x1, y1;
};

std::vector<Cell> Points;
float totalTime = 0.0f;

struct Quad
{
	float t_lX, t_rX, b_lX, b_rX;
	float t_lY, t_rY, b_lY, b_rY;
};

std::vector<Quad>Quads;
int key_index;

//Upravljanje animacijom pomoću tipki
void keyboard(unsigned char key, int, int)
{
	switch (key)
	{
	case 's': case 'S':
		p = 1;
		break;
	case 'x': case 'X':
		p = 0;
		break;
	case 27: //ESCAPE
		exit(0);
		break;
	}
}


float offX = 0.0f;
float offY = 0.0f;
char end = 0;


//Definiranje timera
void timer(int)
{
	if (p && !end)
	{
		offX = offX + 0.1f;
		offY = offY - 0.1f;
	}

	glutTimerFunc(60, timer, 0);
	glutPostRedisplay();
	if (p) totalTime += 0.03f;
}

void init()
{
	GLfloat mat_specular[] = { 0.0f,1.0f,0.0f,1.0f };
	GLfloat mat_shininess[] = { 75.0f };

	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shininess);

	GLfloat light0_ambient[] = { 0.5f,0.5f,0.5f,1.0f };
	GLfloat light0_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
	GLfloat light0_specular[] = { 1.0f,1.0f,1.0f,1.0f };
	GLfloat light0_position[] = { 5.0f,5.0f,5.0f,0.0f };

	glLightfv(GL_LIGHT0, GL_AMBIENT, light0_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light0_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE, light0_ambient);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_NORMALIZE);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void setMaterialColor(GLfloat color[])
{
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, color);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, color);
}

void sfera()
{
	GLfloat mat_ambient_blue[] = { 0.0f,0.0f,1.0f,1.0f };
	setMaterialColor(mat_ambient_blue);
	glPushMatrix();
	glColor3f(1.0, 1.0, 0.0);
	glutSolidSphere(radius, 50, 50);
	glPopMatrix();
}

void display()
{
	int index = -1;
	for (int i = 0; i < (int)Points.size(); ++i)
	{
		index = i;
		if (Points[i].t > totalTime)
		{
			break;
		}
	}

	float interpolate = (totalTime - Points[index - 1].t) /(Points[index].t - Points[index - 1].t);
	if (interpolate>1) interpolate = 1.0;

	float X1 = interpolate*Points[index].x1 + (1.0f -interpolate)*Points[index - 1].x1;
	float Y1 = interpolate*Points[index].y1 + (1.0f -interpolate)*Points[index - 1].y1;

	Quads[Quads.size() - 1].t_rX = X1;
	Quads[Quads.size() - 1].t_rY = Y1 + radius;
	Quads[Quads.size() - 1].b_rX = X1;
	Quads[Quads.size() - 1].b_rY = Y1 - radius;

	if (index != key_index)
	{
		key_index = index;

		Quads[Quads.size() - 1].t_rX = Points[index -1].x1;
		Quads[Quads.size() - 1].t_rY = Points[index -1].y1 + radius;
		Quads[Quads.size() - 1].b_rX = Points[index -1].x1;
		Quads[Quads.size() - 1].b_rY = Points[index -1].y1 - radius;
		Quad temp;
		temp.t_lX = temp.t_rX = Points[index -1].x1;
		temp.t_lY = temp.t_rY = Points[index -1].y1 + radius;
		temp.b_lX = temp.b_rX = Points[index -1].x1;
		temp.b_lY = temp.b_rY = Points[index -1].y1 - radius;
		Quads.push_back(temp);

	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glTranslatef(-1.5, 1.5, 0.0);
	glPushMatrix();
	glTranslatef(X1, Y1, 0.0f);
	sfera();
	glPopMatrix();
	glPushMatrix();

	GLfloat mat_ambient_red[] = { 1.0f,0.0f,0.0f,1.0f }; //boja traga je crvena
		setMaterialColor(mat_ambient_red);
	for (int i = 0; i < (int)Quads.size(); ++i)
	{
		glBegin(GL_QUADS); //izrada traga koristeci quads
		glVertex2f(Quads[i].t_lX, Quads[i].t_lY);
		glVertex2f(Quads[i].t_rX, Quads[i].t_rY);
		glVertex2f(Quads[i].b_rX, Quads[i].b_rY);
		glVertex2f(Quads[i].b_lX, Quads[i].b_lY);
		glEnd();
	}
	glPopMatrix();
	glFlush();
	glutSwapBuffers();
}


void reshape(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (w <= h)
		glOrtho(-2.0, 2.0, -2.0*(GLfloat)h / (GLfloat)w, 2.0* (GLfloat)h / (GLfloat)w, -10.0, 10.0);
	else
		glOrtho(-2.0*(GLfloat)h / (GLfloat)w, 2.0*(GLfloat)h / (GLfloat)w, -2.0, 2.0, -10.0, 10.0);
	glMatrixMode(GL_MODELVIEW);
	glClearAccum(0.0, 0.0, 0.0, 0.5);
	glClear(GL_ACCUM_BUFFER_BIT);
	glLoadIdentity();
}




int main(int argc, char **argv)
{
	float c = 3.0f;

	Cell k;              
	k.t = 0.0f;			 
	k.x1 = c*0.0f;		
	k.y1 = c*0.0f;		
	Points.push_back(k); 

	k.t = 1.0f;			 
	k.x1 = c*0.1f;		 
	k.y1 = c*-1.0f;		 
	Points.push_back(k);

	k.t = 2.0f;
	k.x1 = c*0.3f;
	k.y1 = c*-0.3f;
	Points.push_back(k);

	k.t = 3.0f;
	k.x1 = c*0.6f;
	k.y1 = c*-1.0f;
	Points.push_back(k);

	k.t = 4.0f;
	k.x1 = c*-0.3f;
	k.y1 = c*0.3f;
	Points.push_back(k);

	k.t = 5.0f;
	k.x1 = c*0.1f;
	k.y1 = c*-1.0f;
	Points.push_back(k);

	key_index = 1;
	Quad temp;
	temp.t_lX = temp.t_rX = Points[0].x1;
	temp.t_lY = temp.t_rY = Points[0].y1 + radius;
	temp.b_lX = temp.b_rX = Points[0].x1;
	temp.b_lY = temp.b_rY = Points[0].y1 -radius;
	Quads.push_back(temp);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH |GLUT_ACCUM);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Zadatak 1 ");
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutTimerFunc(0, timer, 0);
	init();
	glutMainLoop();
	return 0;
}

Ovaj projekt sam izradio u Microsoft Visual Studiu 2015 uz dodatak FreeGLUT-a i GL Extension Wrangler-a.
Izradio sam osjenčanu plavu kuglu koja ide po putanji i ostavlja crveni trag.

Programom se upravlja tako da se pokrene animacija sa tipkom s, stopira sa tipkom x, 
a izlazi iz programa sa tipkom ESC kojoj je ASCII vrijednost 27.
Sfera je inicijalizirana na početku i postavljena u gornjem lijevom kutu.
Veličina prozora OpenGL-a je određena na 600x600.

Pri izradi programa puno su mi koristile sljedeće reference:
www.opengl.org/sdk/docs/man2
http://www.sjbaker.org/steve/omniv/opengl_lighting.html
http://stackoverflow.com/questions/7687148/drawing-sphere-in-opengl-without-using-glusphere
http://www.glprogramming.com/red/chapter12.html
http://www.videotutorialsrock.com/
http://www.opengl-tutorial.org/
http://www.youtube.com
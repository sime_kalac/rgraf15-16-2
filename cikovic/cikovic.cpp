#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <vector>
#include <cmath>
#include <unistd.h>

struct lastPositionType{
    float x;
    float y;
    float z;
};

float x_value = 0.1, y_value=2, z_value=0, t = 0.5, sizeFactor=1, x_delta = 0, x_max, z_spin=-8; // Početne vrijednosti, utvrđene testiranjem
bool moveFlag = false;
std::vector<lastPositionType> lastPositionVector;
std::vector<GLfloat> vertexi;
std::vector<GLfloat> normale;
std::vector<GLfloat> texcoords;
std::vector<GLushort> indices;

lastPositionType calculateBezier(lastPositionType tocke[4], float t)
{
    lastPositionType returnVal;
    // Pomoću osnovne formule za Bezierovu krivulju izračunavamo x, y i z koordinate
    returnVal.x = pow((1-t),  3) * tocke[0].x + 3 * t * pow((1-t), 2) * tocke[1].x + 3 * pow(t, 2) * (1-t) * tocke[2].x + pow(t, 3)* tocke[3].x;
    returnVal.y = pow((1-t),  3) * tocke[0].y + 3 * t * pow((1-t), 2) * tocke[1].y + 3 * pow(t, 2) * (1-t) * tocke[2].y + pow(t, 3)* tocke[3].y;
    returnVal.z = pow((1-t),  3) * tocke[0].z + 3 * t * pow((1-t), 2) * tocke[1].z + 3 * pow(t, 2) * (1-t) * tocke[2].z + pow(t, 3)* tocke[3].z;

    return returnVal;

}

void SolidSphere(float radius, unsigned int prsteni, unsigned int polje)
{
    float const R = 1./(float)(prsteni-1);
    float const S = 1./(float)(polje-1);
    int r, s;

    vertexi.resize(prsteni * polje * 3);                  // Primamo radius kocke, te koliko prstena i polja
    normale.resize(prsteni * polje * 3);                  // odnosno "segmenata" crtamo. Što je veći broj
    texcoords.resize(prsteni * polje * 2);                // prstena i segmenata to je sporije crtanje kugle.
    std::vector<GLfloat>::iterator v = vertexi.begin();   // Pomoću funkcija sin i cos dobivamo sve točke
    std::vector<GLfloat>::iterator n = normale.begin();   // kugle, koje ćemo kasnije iskoristiti za njeno
    std::vector<GLfloat>::iterator t = texcoords.begin(); // crtanje.
    for(r = 0; r < prsteni; r++){
        for(s = 0; s < polje; s++) {
            float const y = sin( -M_PI_2 + M_PI * r * R );
            float const x = cos(2*M_PI * s * S) * sin( M_PI * r * R );
            float const z = sin(2*M_PI * s * S) * sin( M_PI * r * R );

            *t++ = s*S;
            *t++ = r*R;

            *v++ = x * radius;
            *v++ = y * radius;
            *v++ = z * radius;

            *n++ = x;
            *n++ = y;
            *n++ = z;
        }
    }

    indices.resize(prsteni * polje * 4);
    std::vector<GLushort>::iterator i = indices.begin();
    for(r = 0; r < prsteni-1; r++)
    {
        for(s = 0; s < polje-1; s++)
        {
            *i++ = r * polje + s;
            *i++ = r * polje + (s+1);
            *i++ = (r+1) * polje + (s+1);
            *i++ = (r+1) * polje + s;
        }
    }
}


void draw(GLfloat x, GLfloat y, GLfloat z)
{
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(x,y,z);
                                                        // Pomoću dobivenih točaka
    glEnableClientState(GL_VERTEX_ARRAY);               // iz gornje funkcije crtamo
    glEnableClientState(GL_NORMAL_ARRAY);               // kuglu, te ju translatiramo
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glVertexPointer(3, GL_FLOAT, 0, &vertexi[0]);
    glNormalPointer(GL_FLOAT, 0, &normale[0]);
    glTexCoordPointer(2, GL_FLOAT, 0, &texcoords[0]);
    glDrawElements(GL_QUADS, indices.size(), GL_UNSIGNED_SHORT, &indices[0]);
    glPopMatrix();
}


void display()
{
    glClearColor(0, 0, 0, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();

    // Funkcija za namještanje kamere
    gluLookAt(9, 5, 15, // Sa ove tocke // Pogled lagano nakošen i gleda prema dolje
              8, 0, 0,  // gledaj na ovu
              0, 1, 0);

    glColor3f(0, 0.4, 0.8); // Postavljamo boju kugle u plavu, ali malo dodajemo
                            // zelene jer inače kugla izgleda neprirodno.

    glPushMatrix();
    const GLfloat ambient[] = {0.0, 0.05, 0.05, 1};            // Započinjemo crtanje kugle;
    glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);               // Postavljamo sljedeće
    const GLfloat diffuse[] = {0.4, 0.5, 0.5};                 // vrijednosti kako bi
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);               // kugla izgledala kao guma,
    const GLfloat specular[] = {0.4, 0.7, 0.7};                // a ne amorfno.
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
    const GLfloat shinyness= 0.078125;
    glMaterialf(GL_FRONT, GL_SHININESS, shinyness * 128.0);

    draw(x_value, y_value, z_value);  // Crtamo kuglu na zadanim koordinatama
    SolidSphere(1, 12, 24);           // Kugla ima radius 1, te 12 prstena s 24 segmenata
    glPopMatrix();

    //draw surface
    glPushMatrix();                 // Crtamo podlogu,
    glColor4f(0.7, 0.7, 0, 0.7);    // odabiremo boju koja je
    glBegin(GL_QUADS);              // djelomično prozirna.
    glVertex3f(-10, -4, 0);
    glVertex3f(50, -4, 0);
    glVertex3f(50, -5, 0);
    glVertex3f(-10, -5, 0);
    glEnd();
    glPopMatrix();


    const GLfloat clear[] = {0.0, 0.0, 0.0, 1};   // "Čistimo" prijašnje
    glMaterialfv(GL_FRONT, GL_AMBIENT, clear);    // vrijednosti kako nam
    glMaterialfv(GL_FRONT, GL_DIFFUSE, clear);    // trag ne bi izgledao
    glMaterialfv(GL_FRONT, GL_SPECULAR, clear);   // isto osjenčano kao i
    glColor3f(0.5, 0, 0);                         // kugla, te postavljamo
                                                  // boju trake na crvenu.
//    glPushMatrix();
//    glLineWidth(2.5);
//    glBegin(GL_LINE_STRIP);                             // Koristimo GL_LINE_STRIP
//                                                        // što zapravo crta liniju
//    for(auto i = 1; i < lastPositionVector.size(); i++) // između svih zadanih točaka
//    {
//        glVertex3f(lastPositionVector[i].x, lastPositionVector[i].y, lastPositionVector[i].z);
//    }                                                   // Dodajemo sve točke koje smo
//                                                        // stavili u vektor
//    glEnd();
//    glPopMatrix();

    glColor3f(0.5, 0, 0);
    for(auto i = 1; i < lastPositionVector.size(); i+=2) // Nažalost zakomentirani kod iznad
    {                                                    // ne možemo koristiti, iako on
        glPushMatrix();                                  // vrlo jednostavno crta trag, no
        glLineWidth(2.5);                                // u zadatku je zadano da crta mora
        glBegin(GL_LINES);                               // biti isprekidana.
        glVertex3f(lastPositionVector[i-1].x, lastPositionVector[i-1].y, lastPositionVector[i-1].z);
        glVertex3f(lastPositionVector[i].x, lastPositionVector[i].y, lastPositionVector[i].z);
        glEnd();                                         // Sam efekt isprekidane linije se dobiva
        glPopMatrix();                                   // tako što se crta svaka druga linija.
    }

    glutSwapBuffers();
    glutPostRedisplay();

}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27:        // U slučaju pritiska tipke ESC
        exit(0);    // (27 je kod za ESC) izlazimo
        break;      // iz programa

    case 'R':       // Moramo dodati i malo i veliko r,
    case 'r':       // u slučaju da je caps lock upaljen
        t=0.5; x_value = 0.1; y_value=2; sizeFactor = 1; x_delta = 0;  // U slučaju pritiska na tipku R
        lastPositionVector.clear();                                    // resetiramo sve vrijednosti te čistimo trag
        break;

    case 'X':
    case 'x':
        moveFlag = 0;
        break;
    case 'S':       // Isto s tipkom S
    case 's':
        moveFlag ^=1; // Radimo binarnu operaciju
        break;        // exclusive or, kako bi napravili
    }                 // "Toggle" variable moveFlag

}
void resize(int w, int h)
{
    if (h == 0) h = 1;
    float ratio = 1.0* w / h;         // Postavljamo perspektivu
    glMatrixMode(GL_PROJECTION);      // u odnosu na dimenzije
    glLoadIdentity();
    gluPerspective(45,ratio,1,1000);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void init() {                   // Postavljamo sve potrebne stvari za pravilno izvršavanje programa
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // Za prozirnost
    glEnable( GL_BLEND );
    glEnable(GL_DEPTH_TEST);    // Testiranje dubine (u slučaju da se nešto preklapa)
    glEnable(GL_LIGHTING);      // Osvijetljenje
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHT0);        // Enable svjetla i pozicioniranje
    float pos[] = {0.7, 0.7, 0.7, 1};
    glLightfv(GL_LIGHT0, GL_POSITION, pos);
}

void idleFunc(){
    if (x_value < x_max && moveFlag){   // Ako je trenutačna X translacija
        t += 0.02;                      // veća od širine ekrana, ili ako je
    }                                   // kugla pauzirana, prekini s pomicanjem
    else                                // kugle.
    {
        moveFlag=false;
    }

    if(t > 1)
    {                                   // Ako smo došli do kraja krivulje,
        t = 0;                          // vrati se na početak krivulje i
        sizeFactor *= 0.9;              // smanji krivulju za idući skok, te
        x_delta = x_value;              // je pomakni da bude nakon zadnje krivulje
    }

    lastPositionType tocke[4];

    tocke[0].x = 0;      tocke[0].y = 0;    z_spin > 0 ? tocke[0].z = z_spin : tocke[0].z = 0;    // Postavljamo točke po kojima računamo krivulju,
    tocke[1].x = 1.60;   tocke[1].y = 5;    z_spin > 0 ? tocke[1].z = --z_spin : tocke[1].z = 0;  // Točke su predodređene testiranjem. Koristimo i
    tocke[2].x = 1.90;   tocke[2].y = 5;    z_spin > 0 ? tocke[2].z = --z_spin : tocke[2].z = 0;  // z os kako bi dobili osječaj spina. Budući da se
    tocke[3].x = 3.5;    tocke[3].y = 0;    z_spin > 0 ? tocke[3].z = --z_spin : tocke[3].z = 0;  // kugla ne pomiče previše po z osi teško je
                                                                                                  // primjetiti spin :(
    lastPositionType calculatedBezier = calculateBezier(tocke, t); // Računamo vrijednosti za trenutačnu točku

    x_value = calculatedBezier.x * sizeFactor + x_delta;  // Za x os dodajemo pomak, ovismo o tome kolika nam je krivulja
    y_value = calculatedBezier.y * sizeFactor - 3;        // Spuštamo cijelu krivulju dolje, da nam je bude lakše opaziti.
    z_value = calculatedBezier.z;

    if(moveFlag){                                          // Ako se krivulja pomiče
        lastPositionVector.push_back(lastPositionType());  // dodajemo zadnje pozicije
        lastPositionVector.back().x = x_value;             // u vektor, koje će biti
        lastPositionVector.back().y = y_value;             // iscrtane sa GL_LINE_STRIP
        lastPositionVector.back().z = z_value;
    }

    usleep(10000);         // Pauza između frameova
    glutPostRedisplay();
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH); // Inicijaliziramo program te postavljamo GLUT_RGBA što znači da
                                                               // omogućavamo prozirnost u programu (inače bi koristili GLUT_RGB)

    glutInitWindowSize(glutGet(GLUT_SCREEN_WIDTH), glutGet(GLUT_SCREEN_HEIGHT) - 50); // Dohvaćamo veličinu ekrana te postavljamo
                                                                                      // dimenzije prozora na dimenzije ekrana
                                                                                      // kako bi se prozor proširio preko ekrana.
                                                                                      // Od visine ekrana oduzimamo 50 kako bi
                                                                                      // kompenzirali za taskbarove i sl.

    x_max = glutGet(GLUT_SCREEN_WIDTH) / 100;   // Budući da uzimamo proizvoljnu veličinu ekrana, kuglica mora skakati do desnog
                                                // ruba ekrana, za maksimalni pomak po x-osi uzimamo širinu ekrana / 100
                                                // (Utvrđeno testitiranjem)

    glutCreateWindow("Racunalna grafika: Edi Cikovic");  // Postavimo naziv ekrana

    init();

    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(resize);

    glutIdleFunc(idleFunc);

    glutMainLoop();
    return 0;
}

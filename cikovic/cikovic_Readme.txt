***Opis rješenja***
Zadatak je realiziran na način da se izračunaju sve koordinate kugle, te se iste zatim i nacrtaju. Materijal kugle određen je pomoću "http://devernay.free.fr/cours/opengl/materials.html", točnije koristeći "cyan rubber".
Budući da se program automatski proširi po cijelom ekranu, duljina skakanja kuglice će ovisiti o širini ekrana. Formula koja određuje do koliko će kuglica skakati, odnosno aproksimira kada je kuglica došla do ruba ekrana je:
Širina ekrana / 100 --> što daje maksimalni pomak kuglice po x-u
(Ova formula je testirana na ekranima širine 1600 i 1920 px)
Pomak kuglice određen je Bezierovom krivuljom koja je određena s 4 točke. Program radi na način da prvo kuglica prijeđe po cijeloj krivulji, te zatim kuglica ponovo prolazi po cijeloj takvoj krivulji, samo što se skalira 10% manje nego zadnji prijelaz, te je pomaknuta za sumu svih prijašnjih širina krivulja.
Isti proces se ponavlja dok kuglica ne dođe do desnog "ruba ekrana" određenog s prije navedenom formulom.
Svaka točka koju prijeđe kugla se sprema u vektor, koji se zatim cijeli crta u obliku linija, što stvara trag. Nakon svakog resetiranja kugle na originalnu poziciju trag se prazni. Budući da se u zadatku traži da je potrebna isprekidana linija, crtamo liniju između svake dvije točke što za efekt daje isprekidanu liniju.

***Kontrole***
Kontrole su kao što je zadano u zadatku:
S - za pokretanje i zaustavljanje kugle
X - zaustavljanje kugle
R - resetiranje kugle
ESC - Izlaz iz programa

***Reference:***
Laboratorijske vježbe
http://devernay.free.fr/cours/opengl/materials.html


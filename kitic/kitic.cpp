#include <iostream>
#include <GL/glut.h>
#include <math.h>

GLfloat whiteSpecularLight[] = {1.0, 1.0, 1.0}; //set the light specular to white
GLfloat blackAmbientLight[] = {0.8, 0.8, 0.8}; //set the  light ambient to grey
GLfloat whiteDiffuseLight[] = {1.0, 1.0, 1.0}; //set the  diffuse light to white
GLfloat whiteSpecularMaterial[] = {1.0, 1.0, 1.0}; //set the material to white
GLfloat mShininess[] = {128};

GLfloat angle = 0.0f;
GLfloat rot_angle = 30;
GLfloat rot_angleb = 10;
bool flagOutline = 0;

//dimenzije case
float z = 0.0;
float x_bottom1 = -0.85, y_bottom1 = 0.0;
float x_bottom2 = -0.85, y_bottom2 = 0.05;
float x_bottom3 = -0.1, y_bottom3 = 0.1;
float x_height = -0.08, y_height = 2.0;
float x_bezierO1 = -0.1, y_bezierO1 = 2.0;
float x_bezierO2 = -1.0, y_bezierO2 = 2.3;
float x_bezierO3 = -1.1, y_bezierO3 = 3.7;
float x_bezierO4 = -0.85, y_bezierO4 = 4.5;
float x_bezierI1 = -0.75, y_bezierI1 = 4.5;
float x_bezierI2 = -1.0, y_bezierI2 = 3.7;
float x_bezierI3 = -0.9, y_bezierI3 = 2.3;
float x_bezierI4 = -0.0, y_bezierI4 = 2.1;
float x_top1 = -0.85, x_top2 = -0.75, y_top = 4.5; 

const int numcontrolpts2D = 4; 
const int numcontrolpts3Du = 4;
const int numcontrolpts3Dv = 4;

GLfloat controlpts2DO[numcontrolpts2D][3] = {
   {x_bezierO1, y_bezierO1, z},
   {x_bezierO2, y_bezierO2, z},
   {x_bezierO3, y_bezierO3, z},
   {x_bezierO4, y_bezierO4, z},
};

GLfloat controlpts2DI[numcontrolpts2D][3] = {
   {x_bezierI1, y_bezierI1, z},
   {x_bezierI2, y_bezierI2, z},
   {x_bezierI3, y_bezierI3, z},
   {x_bezierI4, y_bezierI4, z},
};

GLfloat controlpts3DO[numcontrolpts3Du][numcontrolpts3Dv][3] = {
  {{x_bezierO1, y_bezierO1, z},
   {x_bezierO2, y_bezierO2, z},
   {x_bezierO3, y_bezierO3, z},
   {x_bezierO4, y_bezierO4, z}},
  {{x_bezierO1*cos(1 * rot_angleb * 3.14 / 180), y_bezierO1, x_bezierO1*sin(1 * rot_angleb * 3.14 / 180)},
   {x_bezierO2*cos(1 * rot_angleb * 3.14 / 180), y_bezierO2, x_bezierO2*sin(1 * rot_angleb * 3.14 / 180)},
   {x_bezierO3*cos(1 * rot_angleb * 3.14 / 180), y_bezierO3, x_bezierO3*sin(1 * rot_angleb * 3.14 / 180)},
   {x_bezierO4*cos(1 * rot_angleb * 3.14 / 180), y_bezierO4, x_bezierO4*sin(1 * rot_angleb * 3.14 / 180)}},
  {{x_bezierO1*cos(2 * rot_angleb * 3.14 / 180), y_bezierO1, x_bezierO1*sin(2 * rot_angleb * 3.14 / 180)},
   {x_bezierO2*cos(2 * rot_angleb * 3.14 / 180), y_bezierO2, x_bezierO2*sin(2 * rot_angleb * 3.14 / 180)},
   {x_bezierO3*cos(2 * rot_angleb * 3.14 / 180), y_bezierO3, x_bezierO3*sin(2 * rot_angleb * 3.14 / 180)},
   {x_bezierO4*cos(2 * rot_angleb * 3.14 / 180), y_bezierO4, x_bezierO4*sin(2 * rot_angleb * 3.14 / 180)}},
  {{x_bezierO1*cos(3 * rot_angleb * 3.14 / 180), y_bezierO1, x_bezierO1*sin(3 * rot_angleb * 3.14 / 180)},
   {x_bezierO2*cos(3 * rot_angleb * 3.14 / 180), y_bezierO2, x_bezierO2*sin(3 * rot_angleb * 3.14 / 180)},
   {x_bezierO3*cos(3 * rot_angleb * 3.14 / 180), y_bezierO3, x_bezierO3*sin(3 * rot_angleb * 3.14 / 180)},
   {x_bezierO4*cos(3 * rot_angleb * 3.14 / 180), y_bezierO4, x_bezierO4*sin(3 * rot_angleb * 3.14 / 180)}},
};

GLfloat controlpts3DI[numcontrolpts3Du][numcontrolpts3Dv][3] = {
  {{x_bezierI1, y_bezierI1, z},
   {x_bezierI2, y_bezierI2, z},
   {x_bezierI3, y_bezierI3, z},
   {x_bezierI4, y_bezierI4, z}},
  {{x_bezierI1*cos(1 * rot_angle * 3.14 / 180), y_bezierI1, x_bezierI1*sin(1 * rot_angle * 3.14 / 180)},
   {x_bezierI2*cos(1 * rot_angle * 3.14 / 180), y_bezierI2, x_bezierI2*sin(1 * rot_angle * 3.14 / 180)},
   {x_bezierI3*cos(1 * rot_angle * 3.14 / 180), y_bezierI3, x_bezierI3*sin(1 * rot_angle * 3.14 / 180)},
   {x_bezierI4*cos(1 * rot_angle * 3.14 / 180), y_bezierI4, x_bezierI4*sin(1 * rot_angle * 3.14 / 180)}},
  {{x_bezierI1*cos(2 * rot_angle * 3.14 / 180), y_bezierI1, x_bezierI1*sin(2 * rot_angle * 3.14 / 180)},
   {x_bezierI2*cos(2 * rot_angle * 3.14 / 180), y_bezierI2, x_bezierI2*sin(2 * rot_angle * 3.14 / 180)},
   {x_bezierI3*cos(2 * rot_angle * 3.14 / 180), y_bezierI3, x_bezierI3*sin(2 * rot_angle * 3.14 / 180)},
   {x_bezierI4*cos(2 * rot_angle * 3.14 / 180), y_bezierI4, x_bezierI4*sin(2 * rot_angle * 3.14 / 180)}},
  {{x_bezierI1*cos(3 * rot_angle * 3.14 / 180), y_bezierI1, x_bezierI1*sin(3 * rot_angle * 3.14 / 180)},
   {x_bezierI2*cos(3 * rot_angle * 3.14 / 180), y_bezierI2, x_bezierI2*sin(3 * rot_angle * 3.14 / 180)},
   {x_bezierI3*cos(3 * rot_angle * 3.14 / 180), y_bezierI3, x_bezierI3*sin(3 * rot_angle * 3.14 / 180)},
   {x_bezierI4*cos(3 * rot_angle * 3.14 / 180), y_bezierI4, x_bezierI4*sin(3 * rot_angle * 3.14 / 180)}},
};

int numdrawsegs = 10;        

const int startwinsize = 400; 

void init2DBezier (GLfloat controlpts[numcontrolpts2D][3]) {
    glMap1f(GL_MAP1_VERTEX_3,   
           0.0, 1.0,           
           3,                  
           numcontrolpts2D,      
           &controlpts[0][0]); 
    glEnable(GL_MAP1_VERTEX_3); 

    glMapGrid1d(numdrawsegs, 0.0, 1.0);
}

void init3DBezier (GLfloat controlpts[numcontrolpts3Du][numcontrolpts3Dv][3]) {
    glMap2f(GL_MAP2_VERTEX_3, 
	    0, 1, 3, numcontrolpts3Du,
            0, 1, 12, numcontrolpts3Dv, 
	    &controlpts[0][0][0]);
   glEnable(GL_MAP2_VERTEX_3);
   
   glMapGrid2f(numdrawsegs, 0.0, 1.0, numdrawsegs, 0.0, 1.0);
}

void init (void) {
    glEnable (GL_DEPTH_TEST);
    glEnable (GL_LIGHTING);
    glEnable (GL_LIGHT0);
    
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
}

void light (void) {
    glLightfv(GL_LIGHT0, GL_SPECULAR, whiteSpecularLight);
    glLightfv(GL_LIGHT0, GL_AMBIENT, blackAmbientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDiffuseLight);
    
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, whiteSpecularMaterial);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mShininess);
}

void draw2DBezierCurve (void) {
    glEvalMesh1(GL_LINE, 0, numdrawsegs);
}

void draw3DBezierCurve (void) {
    glEvalMesh2(GL_FILL, 0, numdrawsegs, 0, numdrawsegs);
}

void drawGlassOutline (void) {
    glBegin(GL_LINE_STRIP);
    //dno case
    glVertex3f(0.0, 0.0, z);
    glVertex3f(x_bottom1, y_bottom1, z);
    glVertex3f(x_bottom2, y_bottom2, z);
    glVertex3f(x_bottom3, y_bottom3, z);
    //visina case
    glVertex3f(x_height, y_height, z);
    //vrh case
    glEnd();
    
    //vanjski rub 
    init2DBezier(controlpts2DO);
    draw2DBezierCurve();
	
    //unutranji rub
    init2DBezier(controlpts2DI);
    draw2DBezierCurve();
    
    //vrh case
    glBegin(GL_LINES);
    glVertex3f(x_top1, y_top, z);
    glVertex3f(x_top2, y_top, z);
    glEnd();
}

void drawGlass (void) {
    float yangle=0;
    
    glLineWidth(1);
    glColor4f(0.5, 0.5, 0.5, 0.3);
    
    //dno case
    glBegin(GL_TRIANGLE_FAN);
      glVertex3f(0.0, 0.0, z);
      for(yangle=0; yangle<=360; yangle+=rot_angle){
	glVertex3f(x_bottom1*cos(yangle * 3.14 / 180), y_bottom1, x_bottom1*sin(yangle * 3.14 / 180));
      }
    glEnd();
    
    glBegin(GL_TRIANGLE_STRIP);
      for(yangle=0; yangle<=360; yangle+=rot_angle){
	glVertex3f(x_bottom1*cos(yangle * 3.14 / 180), y_bottom1, x_bottom1*sin(yangle * 3.14 / 180));
	glVertex3f(x_bottom2*cos(yangle * 3.14 / 180), y_bottom2, x_bottom2*sin(yangle * 3.14 / 180));
      }
    glEnd();
    
    glBegin(GL_TRIANGLE_STRIP);
      for(yangle=0; yangle<=360; yangle+=rot_angle){
	glVertex3f(x_bottom2*cos(yangle * 3.14 / 180), y_bottom2, x_bottom2*sin(yangle * 3.14 / 180));
	glVertex3f(x_bottom3*cos(yangle * 3.14 / 180), y_bottom3, x_bottom3*sin(yangle * 3.14 / 180));
      }
    glEnd();
    
    glBegin(GL_TRIANGLE_STRIP);
      for(yangle=0; yangle<=360; yangle+=rot_angle){
	glVertex3f(x_bottom3*cos(yangle * 3.14 / 180), y_bottom3, x_bottom3*sin(yangle * 3.14 / 180));
	glVertex3f(x_height*cos(yangle * 3.14 / 180), y_height, x_height*sin(yangle * 3.14 / 180));
      }
    glEnd();
    
    
    glBegin(GL_TRIANGLE_FAN);
      glVertex3f(0.0, y_height, z);
      for(yangle=0; yangle<=360; yangle+=rot_angle){
	glVertex3f(x_height*cos(yangle * 3.14 / 180), y_height, x_height*sin(yangle * 3.14 / 180));
      }
    glEnd();
    //kraj visine case
    
   for(yangle=0; yangle<360; yangle+=rot_angle){
      //vanjski rub
      init3DBezier(controlpts3DO);
      draw3DBezierCurve();
      
      //unutranji rub
      init3DBezier(controlpts3DI);
      draw3DBezierCurve();
      
      glRotatef(rot_angle,0.0f,1.0f,0.0f);
    }
    
    //vrh case
    glBegin(GL_TRIANGLE_STRIP);
      for(yangle=0; yangle<=360; yangle+=10){
	glVertex3f(x_top1*cos(yangle * 3.14 / 180), y_top, x_top1*sin(yangle * 3.14 / 180));
	glVertex3f(x_top2*cos(yangle * 3.14 / 180), y_top, x_top2*sin(yangle * 3.14 / 180));
      }
    glEnd();
}

void display (void) {
    glClearColor(0.2f, 0.3f, 0.2f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    
    light();
    
    glTranslatef(0,0,-10);
    glRotatef(30,1.0f,0.0f,0.0f);
    
    glRotatef(angle,0.0f,0.0f,1.0f);
   
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    
    if(flagOutline) drawGlassOutline();
    else drawGlass();
    
    glutSwapBuffers();
    
    angle++;
}

void keyboard (unsigned char key, int x, int y) {
    if (key=='w'){
      glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    }
    if (key=='s'){
      glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    }
    if (key=='o'){
      if(flagOutline) flagOutline = 0;
      else flagOutline = 1;
    }
    if (key==27){ // pritisnut esc
      exit (0);
    }
}

void reshape (int width, int height){
    glViewport (0, 0, (GLsizei)width, (GLsizei)height);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluPerspective (60, (GLfloat)width / (GLfloat)height, 1.0, 100.0);
    glMatrixMode (GL_MODELVIEW);
}

int main (int argc, char **argv) {
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize (500, 500);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("Rotating glass");
    init();
    
    glutDisplayFunc (display);
    glutIdleFunc (display);
    glutKeyboardFunc (keyboard);
    glutReshapeFunc (reshape); 
    
    glutMainLoop ();
    return 0;
}

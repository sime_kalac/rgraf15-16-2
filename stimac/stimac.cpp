#include <GL/gl.h>
#include <GL/glut.h>
#include <cmath>

#include <iostream>
#include <Eigen/Dense>
using namespace Eigen;
using namespace std;

GLfloat qaBlack[] = {0.0, 0.0, 0.0, 1.0}; //Black Color
GLfloat qaGreen[] = {0.0, 1.0, 0.0, 1.0}; //Green Color
GLfloat qaWhite[] = {1.0, 1.0, 1.0, 1.0}; //White Color
GLfloat qaRed[] = {1.0, 0.0, 0.0, 1.0}; //White Color
    // Set lighting intensity and color
GLfloat qaAmbientLight[]    = {0.2, 0.2, 0.2, 1.0};
GLfloat qaDiffuseLight[]    = {0.8, 0.8, 0.8, 1.0};
GLfloat qaSpecularLight[]    = {1.0, 1.0, 1.0, 1.0};
GLfloat emitLight[] = {0.9, 0.9, 0.9, 0.01};
GLfloat Noemit[] = {0.0, 0.0, 0.0, 1.0};

    // Light source position
GLfloat qaLightPosition[]    = {5, 5, 5, 0};


float cone_angle = 0;
float dist,px[4],py[4],pz[4];
int operate=0,tocka=0;

bool keyStates[256];
void keyOperations (unsigned char key) 
{	
    if(keyStates[115])operate=1;	//start
    if(keyStates[101])operate=0;	//stop
  
   if(!operate){
//odabrana je tocka 1
   if(keyStates[49])tocka=1;
//odabrana je tocka 2
   if(keyStates[50])tocka=2;
//odabrana je tocka 3   
   if(keyStates[51])tocka=3;

   if (tocka!=0){
     
      if(key=='w')py[tocka]+=0.5; 
      if(key=='x')py[tocka]-=0.5;	
      if(key=='d')px[tocka]+=0.5;	
      if(key=='a')px[tocka]-=0.5;    
  }
   
    if(keyStates[108])cone_angle-= 1; // more left --
    if(keyStates[114])cone_angle+= 1; // more right ++    
    }
     
     if(keyStates[27])exit(0);
}

void keyPressed (unsigned char key, int /*x*/, int /*y*/) 
{
    keyStates[key] = true; 
    keyOperations(key);
}
void keyUp (unsigned char key, int /*x*/, int /*y*/) 
{
    keyStates[key] = false;
}

long binomials ( long n, long k )
{
    // n && k >=0
    long i;
    long b;

    if ( 0 == k || n == k )
    {
        return 1;
    }

    if ( k > n )
    {
        return 0;
    }

    if ( k > ( n - k ) )
    {
        k = n - k;
    }

    if ( 1 == k )
    {
        return n;
    }

    b = 1;

    for ( i = 1; i <= k; ++i )
    {
        b *= ( n - ( k - i ) );

        if ( b < 0 ) return -1; 

        b /= i;
    }

    return b;
}

double polyterm ( const int &n, const int &k, const double &t )
{
    return pow ( ( 1.-t ),n-k ) *pow ( t,k );
}

double getValue ( const double &t, const VectorXd &v )
{
    int order = v.size()-1;
    double value = 0;

    for ( int n=order, k=0; k<=n; k++ )
    {
        if ( v ( k ) ==0 ) continue;

        value += binomials ( n,k ) * polyterm ( n,k,t ) * v ( k );
    }

    return value;
}

void changeSize ( int w, int h )
{
    if ( h == 0 )
    {
        h = 1;
    }

    float ratio = 1.0* w / h;

    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    glViewport ( 0, 0, w, h );
    gluPerspective ( 45,ratio,1,1000 ); 
}

Vector4d pt_x, pt_y, pt_z;
VectorXd t_x, t_y, t_z;
VectorXd ts;
void generateBezierCurve()
{
    ts = VectorXd::LinSpaced ( 21,0.0,1. );
    t_x.resize ( ts.size() );
    t_y.resize ( ts.size() );
    t_z.resize ( ts.size() );
  
    for ( int idx=0; idx< ts.size(); ++idx )
    {
        t_x ( idx ) = getValue ( ts ( idx ), pt_x );
        t_y ( idx ) = getValue ( ts ( idx ), pt_y );
        t_z ( idx ) = getValue ( ts ( idx ), pt_z );
   }
}

void drawBezierCurve()
{
    glPushMatrix();
    glColor3f ( 1.0f, 0.0f, 0.0f );
    glBegin ( GL_LINE_STRIP );
    for ( int i=0; i< ts.size(); ++i )
    { glVertex3f ( t_x ( i ), t_y ( i ), t_z ( i ) ); }
    glEnd();
    glPopMatrix();

}

void Sphere( int lats, int longs) {
       int i, j;
       
	for(i = 0; i <= lats; i++) {
           double lat0 = M_PI * (-0.5 + (double) (i - 1) / lats);
           double z0  = sin(lat0);
           double zr0 =  cos(lat0);
    
           double lat1 = M_PI * (-0.5 + (double) i / lats);
           double z1 = sin(lat1);
           double zr1 = cos(lat1);
   
           glBegin(GL_QUAD_STRIP);
           for(j = 0; j <= longs; j++) {
               double lng = 2 * M_PI * (double) (j - 1) / longs;
               double x = cos(lng);
               double y = sin(lng);
    
               glNormal3f(x * zr0, y * zr0, z0);
               glVertex3f(x * zr0, y * zr0, z0);
               glNormal3f(x * zr1, y * zr1, z1);
               glVertex3f(x * zr1, y * zr1, z1);
           }
           glEnd();
       }
  
}
void drawScene()
{
  
    
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glClearColor (0.3, 0.3, 0.3, 0.0);
    
    glMatrixMode ( GL_MODELVIEW );
    glLoadIdentity(); 
 
    gluLookAt ( 0.0,5.0,10.0, 
                0.0,0.0,-1.0, 
                0.0f,1.0f,0.0f ); 
 float x,y,z;
    
    x = getValue(dist, pt_x);
    y = getValue(dist, pt_y);
    z = getValue(dist, pt_z);
    
   pt_x<< px[0],px[1],px[2],px[3];
   pt_y<< py[0],py[1],py[2],py[3];
   pt_z<< pz[0],pz[1],pz[2],pz[3];
    
    glPushMatrix();
     glRotatef(cone_angle, 0,1,0);
     glPushMatrix();
      glPushMatrix();
       glColor3f(0.5,0.5,0); //siva
	 glTranslatef(x,y,z); 
	  glPushMatrix();
	    glScalef(0.2,0.2,0.2);
	    Sphere(40,40);
	  glPopMatrix();		  
	  glPopMatrix();   
      glPopMatrix();
	generateBezierCurve();	 
	drawBezierCurve();
    glPopMatrix();
    glPopMatrix();
   glutSwapBuffers();
}

float t = 50; 
int loop = 1;

void update ( int /*value*/ )
{  
    glutPostRedisplay();
  
   if (dist==1) operate=0;
   
   if(operate && dist<1)dist+=0.01;  
 
   if(cone_angle > 280 )cone_angle = -90;
    glutTimerFunc ( t, update, 0 );

}

void initLighting()
{

    // Enable lighting
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

     // Set lighting intensity and color
    glLightfv(GL_LIGHT0, GL_AMBIENT, qaAmbientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, qaDiffuseLight);
    glLightfv(GL_LIGHT0, GL_SPECULAR, qaSpecularLight);
    
    // Set the light position
     glLightfv(GL_LIGHT0, GL_POSITION, qaLightPosition);

}

int main ( int argc, char **argv )
{	    
//tocka 1:
px[0]=0;py[0]=0;pz[0]=0;
//tocka 2:
px[1]=1;py[1]=1;pz[1]=0;
//tocka 3:
px[2]=2;py[2]=2;pz[2]=0;
//tocka 4:
px[3]=3;py[3]=0;pz[3]=0;

     for(int i=0; i<256; ++i)
    { keyStates[i] = false; }

    dist = 0.00;
    glutInit ( &argc, argv );
    
    glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize ( 500, 500 );
    
    glutCreateWindow ( "DZ" );
    
    glutReshapeFunc ( changeSize );
    glutDisplayFunc ( drawScene );
    initLighting(); 
    
    glutTimerFunc ( 25, update, 0 );
   
    
    
    glutKeyboardFunc(keyPressed); 
    glutKeyboardUpFunc(keyUp); 
    
    glutMainLoop();
    
    return 0;
}

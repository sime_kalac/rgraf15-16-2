/*
 Program inicijalno pokazuje celičnu kulglu koju je potrebno 
 +lansirati po putanji definiranom s Bezierovom krivuljom. 
 -Model celicne kugle ne smije biti gluSphere
 +kombinacijom tipki regulirati smijer(+) i oblik putanje(-)
 +Pritiskom tipke s se pokrece animacija i traje dok se ne pritisne tipka e
 +pritiskom tipke ESC potrebno je izaci iz programa
 +animacija se zaustavlja kada celicna kugla dosegne krajnju tocku putanje
 +Uključiti osvjetljenje 
 -tijelo koje predstavlja kuglu primjereno osjencati
 
 izvori: 
 -vježbe 4 (tipke), 
 -vježbe 6 (Beziere, smjer, kut ispucavanja)
 -vježbe 7 (template)- lightning
 -https://www.opengl.org/discussion_boards/showthread.php/185186-Blending - iscrtavanje kugle
 -(lightnig position):
 -https://www.cse.msu.edu/~cse872/tutorial3.html
 -xoax.net tutorijali
 -http://www.theasciicode.com.ar/ 
 
 */

#include <GL/gl.h>
#include <GL/glut.h>
#include <cmath>
#include <math.h>
#include <iostream>
#include <Eigen/Dense>
#include <vector>

using namespace Eigen;
using namespace std;

//krivulja određena poljem tocaka umjesto unaprijeda definiranih tocaka
// 4 točke i njihov početni raspored

#define N 4
float _x[N]={0,0.5,1.5,2.5};
float _y[N]={0,3,4,0};
float _z[N]={0,0,0,0};

//kretanje
float smjer = 0;
float dist= 0; 
//tipke
bool keyStates[256];
bool on=0;
//eigen!!
Vector4d pt_x, pt_y, pt_z;
VectorXd t_x, t_y, t_z;
VectorXd ts;

//osvjetljenje
GLfloat redDiffuseMaterial[] = {1.0, 0.0, 0.0}; //set the material to red
GLfloat whiteSpecularMaterial[] = {1.0, 1.0, 1.0}; //set the material to white
GLfloat greenEmissiveMaterial[] = {0.0, 1.0, 0.0}; //set the material to green
GLfloat whiteSpecularLight[] = {1.0, 1.0, 1.0}; //set the light specular to white
GLfloat blackAmbientLight[] = {0.0, 0.0, 0.0}; //set the  light ambient to black
GLfloat whiteAmbientLight[] = {1.0, 1.0, 1.0}; //set the  light ambient to white
GLfloat whiteDiffuseLight[] = {1.0, 1.0, 1.0}; //set the  diffuse light to white
//GLfloat blankMaterial[] = {0.0, 0.0, 0.0}; //set the diffuse  light to white
//GLfloat mShininess[] = {128}; //set the shininess of the material
GLfloat LightPosition[]    = {1, 10, 0, 0}; //x,y,z direction - ako nije 0 steka

void init (void) {
    glEnable (GL_DEPTH_TEST);
    glEnable(GL_SMOOTH);
    glEnable (GL_LIGHTING);
    glEnable (GL_LIGHT0);
    
    //materijal
}

void light (void) {
    glLightfv(GL_LIGHT0, GL_SPECULAR, whiteSpecularLight);
    glLightfv(GL_LIGHT0, GL_AMBIENT, blackAmbientLight);
    glLightfv(GL_LIGHT0, GL_AMBIENT, whiteAmbientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDiffuseLight);
    glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
}

int o=-1;
bool  cone=true;
bool  bezier=true;

void keyOperations (unsigned char key) 
{
  //tipke moraju biti provjeravane u odr poredku (1,2,3,4 ne radi nakon s,e,r)
  if(keyStates[27]){ //esc tipka
      cout<<"ESC"<<endl;
      exit(0);
    }   
   switch(key){
     //case '1' : cout<<"1. tocka"<<endl;o=0;cout<<o<<endl;break; //prva tocka je fiksna
     
     case '2' : cout<<"2. tocka"<<endl;o=1;cout<<o<<endl;break; //odabrana tocka 2
     
     case '3' : cout<<"3. tocka"<<endl;o=2;cout<<o<<endl;break; //odabrana tocka 3
     
     case '4' : cout<<"4. tocka"<<endl;o=3;cout<<o<<endl;break; //odabrana tocka 4
  }
    if (o!=-1)
    {	bezier=1;
	  if(key=='+'){
	  cout<<"pomak gore"<<endl;
	  cout<<o<<endl;
	    _y[o]+=0.5;
	  cout<<_y[o]<<endl;    
	  }
	  
	  if(key=='-'){
	  cout<<"pomak dolje"<<endl;
	    _y[o]-=0.5;	
	  }
	
	  if(key=='l'){
	  cout<<"pomak lijevo"<<endl;
	    _x[o]+=0.5;	
	  }
	  if(key=='k'){
	  cout<<"pomak desno"<<endl;
	    _x[o]-=0.5;	
	  }
      
    
    }
    
   if((key=='y') || (key=='Y')) { 
      cout<<"CW"<<endl;
       smjer -= 1;
    }else if((key=='x')|| (key=='X')) { 
      cout<<"CCW"<<endl;
       smjer += 1;
    }else if((key=='s')||(key=='S')){ 
      cout<<"start"<<endl;
      on=1;
    } else if((key=='r')||(key=='R')){
      cout<<"restart"<<endl;
      dist=0;
    }if((key=='e')||(key=='E')){
      on=0;
      cout<<"end"<<endl;
       }
       
     if(key=='c'|| key=='C'){
       
       if(cone){
	 cone=0; 
      }else cone=1; 
      
       
    }
    
    if(key=='b'||key=='b'){
      
      if(bezier){
	 bezier=0; 
      }else bezier=1; 
      
    }
    
    //tipka lijevo 37 tipka desno 39 //ne radi
   
    
    
}
void keyPressed (unsigned char key, int /*x*/, int /*y*/) 
{
    keyStates[key] = true; 
    //cout<<key<<" key pressed"<<endl;
    keyOperations(key);
    
}

//void keyUp (unsigned char key) 
void keyUp (unsigned char key, int /*x*/, int /*y*/) 
{
    keyStates[key] = false;
}

long binomials ( long n, long k )
{
    // n && k >=0
    long i;
    long b;

    if ( 0 == k || n == k )
    {
        return 1;
    }

    if ( k > n )
    {
        return 0;
    }

    if ( k > ( n - k ) )
    {
        k = n - k;
    }

    if ( 1 == k )
    {
        return n;
    }

    b = 1;

    for ( i = 1; i <= k; ++i )
    {
        b *= ( n - ( k - i ) );

        if ( b < 0 ) return -1; /* ERR!!!! OVERFLOW */

        b /= i;
    }

    return b;
}

double polyterm ( const int &n, const int &k, const double &t )
{
    return pow ( ( 1.-t ),n-k ) *pow ( t,k );
}

double getValue ( const double &t, const VectorXd &v )
{
    int order = v.size()-1;
    double value = 0;

    for ( int n=order, k=0; k<=n; k++ )
    {
        if ( v ( k ) ==0 ) continue;

        value += binomials ( n,k ) * polyterm ( n,k,t ) * v ( k );
    }

    return value;
}

void changeSize ( int w, int h )
{
    if ( h == 0 )
    {
        h = 1;
    }

    float ratio = 1.0* w / h;

    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    glViewport ( 0, 0, w, h );
    gluPerspective ( 45,ratio,1,1000 ); // view angle u y, aspect, near, far
}


void drawControlPoints()
{
    glPushMatrix();
    glColor3f ( 1.0f, 1.0f, 1.0f );
    glBegin ( GL_LINE_STRIP );
    
    glVertex2f ( pt_x ( 0 ), pt_y ( 0 ));
    glVertex2f ( pt_x ( 1 ), pt_y ( 1 ));
    glVertex2f ( pt_x ( 2 ), pt_y ( 2 ));
    glVertex2f ( pt_x ( 3 ), pt_y ( 3 ));
    
    
    glEnd();
    glPopMatrix();
}

void generateBezierCurve()
{
    ts = VectorXd::LinSpaced ( 21,0.0,1. ); 
    ts = VectorXd::LinSpaced ( 21,0.0,1. ); 
   
    // 21 je broj točaka 
    //1 je dužina crte 
    //0 je od koje udaljenosti krece iscrtavanje krivulje (x)
   
    t_x.resize ( ts.size() );
    t_y.resize ( ts.size() );
    t_z.resize ( ts.size() );
  
    for ( int idx=0; idx< ts.size(); ++idx )
    {
        t_x ( idx ) = getValue ( ts ( idx ), pt_x );
        t_y ( idx ) = getValue ( ts ( idx ), pt_y );
        t_z ( idx ) = getValue ( ts ( idx ), pt_z );
	
	/*cout<<"idx=";
	 cout<<idx<<endl;//0-21
	 cout<<"t_y(idx)=";
 	 cout<<t_y(idx)<<endl;*/  
      
    }
}

void drawBezierCurve()
{
   
    glPushMatrix();
   
    //glColor3f ( 1.0f, 0.0f, 0.0f ); //RGB
    
    glBegin ( GL_LINE_STRIP );
    for ( int i=0; i< ts.size(); ++i )
    { 
      glVertex3f ( t_x ( i ), t_y ( i ), t_z ( i ) ); }
    
    glEnd();
    glPopMatrix();

}
void solidSphere(double /*r*/, int lats, int longs) {
       int i, j;
       
	for(i = 0; i <= lats; i++) {
           double lat0 = M_PI * (-0.5 + (double) (i - 1) / lats);
           double z0  = sin(lat0);
           double zr0 =  cos(lat0);
    
           double lat1 = M_PI * (-0.5 + (double) i / lats);
           double z1 = sin(lat1);
           double zr1 = cos(lat1);
   
           glBegin(GL_QUAD_STRIP);
           for(j = 0; j <= longs; j++) {
               double lng = 2 * M_PI * (double) (j - 1) / longs;
               double x = cos(lng);
               double y = sin(lng);
    
               glNormal3f(x * zr0, y * zr0, z0);
               glVertex3f(x * zr0, y * zr0, z0);
               glNormal3f(x * zr1, y * zr1, z1);
               glVertex3f(x * zr1, y * zr1, z1);
           }
           glEnd();
       }
       
      // glPopMatrix();

  
}

void drawCone()
{
  if(cone){
    glPushMatrix();
	glRotatef(90, 0,1,0);
	//glColor3f ( 0.0f, 1.0f, 0.0f );
	glutWireCone(0.2, 1, 16,4);
    glPopMatrix();
  }
}
/*
void drawCircle(float radius)
{
   glBegin(GL_LINE_LOOP);
  for(int i =0; i <= 300; i++){
    double angle = 2 * 3.14 * i / 300;
    double x = cos(angle);
    double y = sin(angle);
   glVertex2d(x,y);
}
  glEnd();
}
*/

void drawScene()
{	
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glClearColor (0.3, 0.3, 0.3, 0.0);
    
    glMatrixMode ( GL_MODELVIEW ); // idemo u perspektivu
    glLoadIdentity(); // resetiranje
    
    /*
    glMatrixMode(GL_MODELVIEW);
    // clear the drawing buffer.
    glClear(GL_COLOR_BUFFER_BIT);
  
   */ 
//     cout<<"x=";
//     cout<<x<<endl;
//     cout<<"y=";
//     cout<<y<<endl;
//     cout<<"z=";
//     cout<<z<<endl;
//     
    gluLookAt ( 0.0,5.0,10.0, // camera
                0.0,0.0,-1.0, // where
                0.0f,1.0f,0.0f ); // up vector

    glPushMatrix();
	
        glRotatef(smjer,0,1,0);
        drawCone();
            glPushMatrix();
  
   pt_x<< _x[0],_x[1],_x[2],_x[3];
   pt_y<< _y[0],_y[1],_y[2],_y[3];

		if(bezier)drawControlPoints();		   
		generateBezierCurve();
		if(bezier)drawBezierCurve();
	      
	      glPushMatrix();
	      glTranslatef(getValue(dist, pt_x),getValue(dist, pt_y),getValue(dist, pt_z));
		  glPushMatrix();
		    light();
		    glColor3f(0.5,0.5,0.5);
		    glScalef(0.5,0.5,0.5);
		    solidSphere(2,40,40);
		   glPopMatrix();
		  
		glPopMatrix();   
	    glPopMatrix();
       glPopMatrix();
 
    glutSwapBuffers();
}
float t = 100; 

void update ( int /*value*/ )
{  
    glutPostRedisplay();
    if(on){
    //dist = 1 - cone_angle/(0-90); //polozaj na krivulji
    if(dist<1){ 
    dist+=00.1;}
      
    if(smjer > 280 ){smjer = -90;}
    //update glut-a nakon 25 ms
    if (dist>1) on=0; //dist=0;
    }
    glutTimerFunc ( t, update, 1 );

}

int main ( int argc, char **argv )
{
     for(int i=0; i<256; ++i)
    { keyStates[i] = false; }
    
    // 4 početne tocke krivulje  
    pt_x << 0, 0.25, 0.6, 0.95;
    //pt_y << 0, 0.8, 0.6, -0.5;
    //pt_z << 0, 0, 0, 0;
    //generateBezierCurve();
  
//	sphere_coo << pt_x(0), pt_y(0), pt_z(0);
//	cube_coo << 0, 0, 0;
    glutInit ( &argc, argv );
    
    glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize ( 600, 600 );
    glutCreateWindow ( "Zadaca- Turk" );   
    glutReshapeFunc ( changeSize );
    init();
    glutDisplayFunc ( drawScene );
    glutTimerFunc ( 25, update, 0 );
    glShadeModel (GL_SMOOTH);
   // glShadeModel (GL_FLAT);
    glutKeyboardFunc(keyPressed); 
    glutKeyboardUpFunc(keyUp); 
    
   // cout<<"ne vrtimo se u krug";
    glutMainLoop();
    
    return 0;
}

Zadatak: Izraditi sferu plave boje, koja će prvotno biti smještena u gornjem lijevom kutu, a zatim pritiskom na određenu tipku
započinje kretanje po bezier spline-u, odnosno bezier spline-ovima. Za sobom pušta trag tamnocrvene boje.

Izrađeno: Kod je dobro komentiran, stoga nema potrebe višestruko opisivati. Ostvatio sam većinu zadanih funkcionalnosti, izuzevši 
zaustavljanje u donjem desnom kutu. Dodao sam i neke dodatne funkcionalnosti i promjenio većinu parametara kako bi poboljšao 
doživljaj animacije.

Upravljanje tipkama:
- S - pokretanje animacije
- X - zaustavljanje animacije
- B - pokaži kontolne točke bezier spline-a
- M - sakrij kontrolne točke bezier spline-a
- C - pokaži bezier spline
- Z - sakrij bezier spline

Korišteni izvori:
-vježbe kolegija Računalna grafika
-http://stackoverflow.com/questions/34034802/moving-along-bezier-curve-in-processing
-https://www.opengl.org/discussion_boards/archive/index.php/t-135618.html
-http://stackoverflow.com/questions/7687148/drawing-sphere-in-opengl-without-using-glusphere
-http://www.cs.uml.edu/~kdaniels/courses/GEOM_580_S11/Curve2D.cpp
-http://stackoverflow.com/questions/5443653/opengl-coordinates-from-bezier-curves
-http://www.glprogramming.com/red/chapter12.html
-https://www.cse.msu.edu/~cse872/tutorial5.html
-http://www.opengl-tutorial.org/

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <GL/glut.h>

#define PI 3.14159265

static int pokreni_animaciju = 0; 
static int trajanje_animacije = 1000; 
using namespace std;
static char theStringBuffer[10]; 
static long font = (long)GLUT_BITMAP_9_BY_15; 
static float vrijeme = 0.0; 
static float sirina = 1.0; 
static float visina = 2.0; 
static float g = 1.0; 

void drawSphere() {
   	int i;
   	float x=0.0, y=0.0, radius=5;
	int triangleAmount = 200; 
	
	
	GLfloat twicePi = 2.0f * PI;
	
	glBegin(GL_TRIANGLE_FAN);
		glVertex2f(x, y);
		for(i = 0; i <= triangleAmount;i++) { 
			glColor3f(0.1, 0.1, 0.1);
			glVertex2f(
		            x + (radius * cos(i *  twicePi / triangleAmount)), 
					y + (radius * sin(i * twicePi / triangleAmount))
			);
		}
	glEnd();
} 

void resize(int w, int h)
{
   glViewport(0, 0, (GLsizei)w, (GLsizei)h); 
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glFrustum(-5.0, 5.0, -5.0, 5.0, 5.0, 100.0);
   glMatrixMode(GL_MODELVIEW);
}

void writeBitmapString(void *font, char *string)
{  
   char *c;

   for (c = string; *c != '\0'; c++) glutBitmapCharacter(font, *c);
}

void floatToString(char * destStr, int precision, float val) 
{
   sprintf(destStr,"%f",val);
   destStr[precision] = '\0';
}

void writeData(void)
{
   glColor3f(1.0, 1.0, 1.0);
   
   floatToString(theStringBuffer, 4, sirina);
   glRasterPos3f(-4.5, 4.5, -5.1);
  
   writeBitmapString((void*)font, theStringBuffer);
   
   floatToString(theStringBuffer, 4, visina);
   glRasterPos3f(-4.5, 4.2, -5.1);
   
   writeBitmapString((void*)font, theStringBuffer);
}

void drawScene(void)
{
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glClearColor(0.0, 0.0, 0.0, 1.0f);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();

   writeData();

   glTranslatef(-15.0, -15.0, -25.0);

   glTranslatef(sirina*vrijeme, visina*vrijeme - (g/2.0)*vrijeme*vrijeme, 0.0);

   glColor3f(0.7, 0.7, 0.7);
   drawSphere();

   glutSwapBuffers();
}

void animate(int)
{
   if (pokreni_animaciju) 
   {
      vrijeme += 0.1;
   }
   glutTimerFunc(trajanje_animacije, animate, 1);
   glutPostRedisplay();
}

void setup(void) 
{
  GLfloat diffuseMaterial[] =  {0.85f, 0.5f, 0.25f}; 
  GLfloat specularMaterial[] = {1.0f,  1.0f, 1.0f};  
  GLfloat ambientMaterial[] =  {0.2f, 0.2f, 0.2f};   
  GLfloat mShininess[]       = {128.0f};			

  GLfloat whiteSpecularLight[] = {1.0, 1.0, 1.0}; 
  GLfloat whiteDiffuseLight[]  = {1.0, 1.0, 1.0}; 
  GLfloat whiteAmbientLight[]  = {1.0, 1.0, 1.0}; 
  GLfloat positionLight[]      = {0.0f, 0.0f, -5.0f, 1.0f};	

  glEnable (GL_DEPTH_TEST);
  glEnable(GL_NORMALIZE);
  glShadeModel(GL_SMOOTH);
  glEnable (GL_LIGHTING);
  glEnable (GL_LIGHT0);
  glLightfv(GL_LIGHT0, GL_SPECULAR, whiteSpecularLight);
  glLightfv(GL_LIGHT0, GL_AMBIENT, whiteAmbientLight);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDiffuseLight);
  glLightfv(GL_LIGHT0, GL_POSITION, positionLight);
}

void keyInput(unsigned char tipka, int, int)
{
   if (tipka=='s' | tipka=='S') 
   {
       pokreni_animaciju = true;
       glutPostRedisplay();
   }
   else if (tipka=='e' | tipka=='E')
   { 
      pokreni_animaciju = false;
      glutPostRedisplay();
   }
   else if (tipka==27)
   {	  
         exit(0);
   }
}

void specialKeyInput(int tipka, int, int)
{
   if(tipka == 101) visina += 0.1;
   if(tipka == 103) {
	 if (visina > 0.1) visina -= 0.1;
	}
   if(tipka == 102) sirina += 0.05; 
   if(tipka == 100) {
	 if (sirina > 0.1) sirina -= 0.05;
	}  
   glutPostRedisplay();
}

int main(int argc, char **argv) 
{
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
   glutInitWindowSize(750, 750);
   glutInitWindowPosition(0, 200); 
   glutCreateWindow ("AJMO KUGLO"); 
   setup(); 
   glutDisplayFunc(drawScene); 
   glutReshapeFunc(resize);  
   glutKeyboardFunc(keyInput);
   glutTimerFunc(5, animate, 1);
   glutSpecialFunc(specialKeyInput);
   glutMainLoop(); 

   return 0;  
}
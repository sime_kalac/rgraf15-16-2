#include <GL/gl.h>
#include <GL/glut.h>
#include <cmath>
#include <math.h>
#include <iostream>
#include <Eigen/Dense>
#include <vector>

using namespace Eigen;
using namespace std;
float cone_angle = -90; //pocetna pozicija kugle
float dist; //udaljenost na krivulji
bool is_started; //varijabla za stanje programa
int win_id; //id glut window-a

void material()
{
	const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
	const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
	const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	const GLfloat light_position[] = { 2.0f, 5.0f, 5.0f, 0.0f };

	const GLfloat mat_ambient[]    = { 0.8f, 0.8f, 0.7f, 1.0f };
	const GLfloat mat_diffuse[]    = { 0.2f, 0.2f, 0.2f, 1.0f };
	const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
	const GLfloat high_shininess[] = { 100.0f };

	glClearColor(0,0,0,0);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);

        glEnable(GL_LIGHT0);
        glEnable(GL_NORMALIZE);
        glEnable(GL_COLOR_MATERIAL);
        glEnable(GL_LIGHTING);

	//osvjetljenje
        glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
        glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
        glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
        glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	//osjencanje
        glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
        glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);

}

long binomials ( long n, long k )
{
    // n && k >=0
    long i;
    long b;

    if ( 0 == k || n == k ) {
        return 1;
    }

    if ( k > n ) {
        return 0;
    }

    if ( k > ( n - k ) ) {
        k = n - k;
    }

    if ( 1 == k ) {
        return n;
    }

    b = 1;

    for ( i = 1; i <= k; ++i ) {
        b *= ( n - ( k - i ) );

        if ( b < 0 )
		return -1; /* ERR!!!! OVERFLOW */
        b /= i;
    }
    return b;
}

double polyterm ( const int &n, const int &k, const double &t )
{
    return pow ( ( 1.-t ),n-k ) *pow ( t,k );
}

double getValue ( const double &t, const VectorXd &v )
{
    int order = v.size()-1;
    double value = 0;

    for ( int n=order, k=0; k<=n; k++ )
    {
        if ( v ( k ) ==0 ) continue;

        value += binomials ( n,k ) * polyterm ( n,k,t ) * v ( k );
    }

    return value;
}

void changeSize ( int w, int h )
{
    if ( h == 0 )
    {
        h = 1;
    }

    float ratio = 1.0* w / h;

    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    glViewport ( 0, 0, w, h );
    gluPerspective ( 45,ratio,1,1000 ); // view angle u y, aspect, near, far
}

Vector4d pt_x, pt_y, pt_z;
VectorXd t_x, t_y, t_z;
VectorXd ts;
void drawControlPoints()
{
    glPushMatrix();
    glBegin ( GL_LINE_STRIP );
    glVertex3f ( pt_x ( 0 ), pt_y ( 0 ), pt_z ( 0 ) );
    glVertex3f ( pt_x ( 1 ), pt_y ( 1 ), pt_z ( 1 ) );
    glVertex3f ( pt_x ( 2 ), pt_y ( 2 ), pt_z ( 2 ) );
    glVertex3f ( pt_x ( 3 ), pt_y ( 3 ), pt_z ( 3 ) );
    glEnd();
    glPopMatrix();
}

void generateBezierCurve()
{
    ts = VectorXd::LinSpaced ( 21,0,1. );
    t_x.resize ( ts.size() );
    t_y.resize ( ts.size() );
    t_z.resize ( ts.size() );

    for ( int idx=0; idx< ts.size(); ++idx )
    {
        t_x ( idx ) = getValue ( ts ( idx ), pt_x );
        t_y ( idx ) = getValue ( ts ( idx ), pt_y );
        t_z ( idx ) = getValue ( ts ( idx ), pt_z );
    }
}

void drawBezierCurve()
{
    glPushMatrix();
    glColor3f ( 1.0f, 0.0f, 0.0f );
    glBegin ( GL_LINE_STRIP );
    for ( int i=0; i< ts.size(); ++i ) { 
	glVertex3f ( t_x ( i ), t_y ( i ), t_z ( i ) ); 
    }
    glEnd();
    glPopMatrix();

}

Vector3d sphere_coo;

void drawSphere()
{
    float x,y,z;
    x = getValue(dist, pt_x);
    y = getValue(dist, pt_y);
    z = getValue(dist, pt_z);
    glPushMatrix();
    glTranslatef(x,y,z);

    glColor3d(0.4,0.4,0.4); //boja sfere siva
    glutSolidSphere(0.5, 50,50); //sfera sa radius 0.5

    glPopMatrix();
    
}

void drawScene()
{
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode ( GL_MODELVIEW ); // idemo u perspektivu
    glLoadIdentity(); // resetiranje

    gluLookAt ( 0.0,0.0,8.0, // camera
                0.0,0.0,-0.5, // where
                0.0f,1.0f,0.0f ); // up vector

    glPushMatrix();
    	drawSphere(); // crtanje kugle
	drawBezierCurve(); // crtanje bezierove krivulje
    glPopMatrix();
    
    glutSwapBuffers();
}

void update (int  )
{
    glutPostRedisplay();
    generateBezierCurve();
    if (is_started) //pomici kuglu ako je tipka stisnuta
    	cone_angle += 1;

    dist = 1 - cone_angle/(0-90);

    if (cone_angle > 0) //zaustavi kada dodjemo do kraja
	is_started = false;

    //update glut-a nakon 25 ms
    glutTimerFunc ( 25, update, 0 );
}

void gl_key_control(int key, int, int)
{
    switch(key) //mjenjanje oblika putanje
    {
        case GLUT_KEY_LEFT:
            pt_x(1) -= 0.1;
            break;
        case GLUT_KEY_RIGHT:
            pt_x(1) += 0.1;
            break;
        case GLUT_KEY_UP:
            pt_y(1) += 0.1;
            break;
        case GLUT_KEY_DOWN:
            pt_y(1) -= 0.1;
            break;
      }
    glutPostRedisplay();
}

void key_control(unsigned char key, int, int)
{
    switch(key)
    {
	case 's': //start tipka
		is_started = true;
		break;
	case 'e': //stop tipka
		is_started = false;
		break;
	case 'r': //restart tipka
		is_started = true;
		cone_angle = -90;
		break;
	case 27: // stisnuli smo esc tipku
		glutDestroyWindow (win_id);
		exit (0);
		break;
      }
    glutPostRedisplay();
}

int main ( int argc, char **argv )
{
    pt_x << -2.5, -0.25, 0.6, 2.95;
    pt_y << -2.5, 0.8, 0.6, -0.5;
    pt_z << 0, 0, 0, 0;
    dist = 0.;

    generateBezierCurve();

    sphere_coo << pt_x(0), pt_y(0), pt_z(0);
    glutInit ( &argc, argv );
    glutInitDisplayMode ( GLUT_DOUBLE /*| GLUT_RGB | GLUT_DEPTH */);
    glutInitWindowSize ( 800, 800 );
    win_id = glutCreateWindow ( "Zadatak 4 - Vlasic: kugla po Bezier krivulji" );
    glutReshapeFunc ( changeSize );
    glutDisplayFunc ( drawScene );
    material();
    glutTimerFunc ( 25, update, 0 );
    glutSpecialFunc(gl_key_control);
    glutKeyboardFunc(key_control);
    glutMainLoop();

    return 0;
}

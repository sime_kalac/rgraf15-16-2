Andrej Vlašić - zadatak 4

Za pomicanje točke Bezierove krivulje se koriste tipke:
	Strelica gore: pomicanje u pozitivnom smjeru y osi.
	Strelica dolje: pomicanje u negativnom smjeru y osi.
	Strelica lijevo: pomicanje u negativnom smjeru x osi.
	Strelica desno: pomicanje u pozitivnom smjeru x osi.

Animacija se pokreče pritiskop tipke "s", i zaustavlja tipkom "e".
Ponovno pokretnaje animacije vrši se tipkom "r".
Za izlaz is programa koristi se tipa "Esc".

Reference za kod sam koristio:
	-vježbe 4 za Bezierovu krivulju, i vježbu 7 za osvjetljenje.
	-https://www.opengl.org/wiki
	-razni primjeri sa stackoverflow.com

Za ispravan compile potrebno je imati eigen 3.2.6 library.

/*
 Julian Čop,
 zadatak 1.
 
 Odskakivanje loptice
 
 */

#include <ctype.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <GL/glut.h>
#include <math.h>
#include <string.h>
#include <iostream>
#include <Eigen/Dense>
using namespace Eigen;
using namespace std;

/* SVIJETLO  */
GLfloat ambientLight[]  = { 0.5, 0.7, 0.7, 1.0 };
GLfloat whiteSpecMate[] = { 1.0, 1.0, 1.0 };
GLfloat positionLight[] = { 4.0, 10.0, 2.0, 1.0 };
GLfloat shininess[]     = { 80 };

/* VARIJABLA ZA POKRETANJE ANIMACIJE (false - pause, true - start) */
bool animacija = false;

/* TIPKE  */
unsigned char start   = 's';
unsigned char stop    = 'x';
unsigned int  end_key     = 27;

/* TRAG */
vector<float> PATHX;
vector<float> PATHY;

/* VEKTORI  (za određivanje položaja)*/
Vector4d pt_x, pt_y, pt_z;
VectorXd t_x, t_y, t_z;
VectorXd ts;

/* VARIJABLE (koristene kod funkcije update za generiranje Bezierovih krivulja) */
float dist=0;
float t=0;
float brojac=0;





/*------------------------------FUNKCIJE:  -----------------------------------*/

long binomials ( long n, long k )
{   // n && k >=0
    long i;
    long b;
    if ( 0 == k || n == k )
    {return 1;  }

    if ( k > n )
    {return 0;  }

    if ( k > ( n - k ) )
    { k = n - k;  }

    if ( 1 == k )
    { return n;   }

    b = 1;

    for ( i = 1; i <= k; ++i )
    {
        b *= ( n - ( k - i ) );
        if ( b < 0 ) return -1;    /* OVERFLOW */
        b /= i;
    }

    return b;
}

double polyterm ( const int &n, const int &k, const double &t )
{
    return pow ( ( 1.-t ),n-k ) *pow ( t,k );
}

double getValue (float t, const VectorXd &v )
{  
    int order = v.size()-1;
    double value = 0;

    if(t>1)
    {   t=0.;  }
    
    for ( int n=order, k=0; k<=n; k++ )
    {
        if ( v ( k ) ==0 ) continue;
        value += binomials ( n,k ) * polyterm ( n,k,t ) * v ( k );
    }
    return value;
}


void changeSize ( int w, int h )
{
    if ( h == 0 )
    {  h = 1;  }

    float ratio = 1.0* w / h;

    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    glViewport ( 0, 0, w, h );
    gluPerspective ( 45,ratio,1,1000 ); // view angle u y, aspect, near, far
}


//crtanje 4 kontrolne tocke prema kojima bismo kasnije mogli napraviti Bezierovu krivulju
void drawControlPoints()
{
    glPushMatrix();
    glColor3f ( 1.0f, 1.0f, 1.0f );
    glBegin ( GL_LINE_STRIP );
    glVertex3f ( pt_x ( 0 ), pt_y ( 0 ), pt_z ( 0 ) );
    glVertex3f ( pt_x ( 1 ), pt_y ( 1 ), pt_z ( 1 ) );
    glVertex3f ( pt_x ( 2 ), pt_y ( 2 ), pt_z ( 2 ) );
    glVertex3f ( pt_x ( 3 ), pt_y ( 3 ), pt_z ( 3 ) );
    glEnd();
    glPopMatrix();
}

//stvaranje Bezierove krivlje na osnovi danih koordinata pt_x, pt_y, pt_z
void generateBezierCurve()
{
    ts = VectorXd::LinSpaced ( 21,0,1. );
    t_x.resize ( ts.size() );
    t_y.resize ( ts.size() );
    t_z.resize ( ts.size() );

    for ( int idx=0; idx< ts.size(); ++idx )
    {
        t_x ( idx ) = getValue ( ts ( idx ), pt_x );
        t_y ( idx ) = getValue ( ts ( idx ), pt_y );
        t_z ( idx ) = getValue ( ts ( idx ), pt_z );
    }
}

//crtanje Bezierove krivulje (kako bismo ih lakse modificirali)
void drawBezierCurve()
{
    glPushMatrix();
    glColor3f ( 1.0f, 0.0f, 0.0f );
    glBegin ( GL_LINE_STRIP );
    for ( int i=0; i< ts.size(); ++i )
    { glVertex3f ( t_x ( i ), t_y ( i ), t_z ( i ) ); }
    glEnd();
    glPopMatrix();

}


//funkcija za crtanje podloge odnosno tla
void drawGround()
{   
    glPushMatrix();
      glBegin(GL_QUAD_STRIP);
	    glColor3f ( 0.0f, 0.0f, 0.0f );
	    glVertex3f(-10, -8, -1);
	    glVertex3f(-10, -0.8, 1);
	    glVertex3f(15, -8, -1);
	    glVertex3f(15, -0.8, 1);
      glEnd();
    glPopMatrix();
}


//funkcija za crtanje putanje odnosno traga za loptom
void drawPath(float x, float y) {
  
    GLfloat whiteSpecMate2[] = { 0, 0, 0 };
    GLfloat shininess2[]     = { 0 };
   
  //dodavanje vrijednosti od ulaznih x i y koordinate
    PATHX.push_back (x);
    PATHY.push_back (y);
    
    glBegin(GL_QUADS);
    
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,  whiteSpecMate2);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess2);
   
    for (unsigned int i = 0; i < PATHX.size(); i++) {
      glColor4f(0.4f, 0.0f, 0.0f, 0.0f);
  
		glNormal3f(PATHX[i]-1.0f, PATHY[i], 0);
		glVertex3f(PATHX[i]-1.0f, PATHY[i], 0);
		glVertex3f(PATHX[i]-1.0f, PATHY[i], 0);
		glVertex3f(PATHX[i]+0.1f-1.0f, PATHY[i]+0.3f, 0);
		glVertex3f(PATHX[i]+0.1f-1.0f, PATHY[i]-0.3f, 0); 
	
    }
    glEnd();
} 


//funkcija za crtanje i ispis lopte 
void drawBall(int lats, int longs,float x, float y) {
    int i, j;
    float z;
    
    z = getValue(dist, pt_z);
    float angle = x / (2 * 3.14)  * 360;   //postavljanje kuta za rotaciju
    glPushMatrix();
    glTranslatef(x,y,z);                   //postavljanje lopte na određene koordinate x i y
    glRotatef(angle, 0, 0, -1);
    
    for(i = 0; i <= lats; i++) {
       double lat0 = M_PI * (-0.5 + (double) (i - 1) / lats);
       double z0  = sin(lat0);
       double zr0 =  cos(lat0);

       double lat1 = M_PI * (-0.5 + (double) i / lats);
       double z1 = sin(lat1);
       double zr1 = cos(lat1);

       glBegin(GL_QUAD_STRIP);
       glColor4f(0, 0, 0.5, 1);
       for(j = 0; j <= longs; j++) {
           double lng = 2 * M_PI * (double) (j - 1) / longs;
           double x = cos(lng);
           double y = sin(lng);
	    
           glNormal3f(x * zr0, y * zr0, z0);
           glVertex3f(x * zr0, y * zr0, z0);
           glNormal3f(x * zr1, y * zr1, z1);
           glVertex3f(x * zr1, y * zr1, z1);
       }
       glEnd();
   }
   glPopMatrix();
 }

 
//funkcija za tipke
void tipke(unsigned char key, int /*x*/, int /*y*/) {
    
     //ako je stisnuta tipka end_key
    if (key == end_key) {
        exit(0);
    }
    //ako je stisnuta tipka start
    if (tolower(key) == start) {
        animacija = true;
      
    }
    //ako je stisnuta tipka stop
    if (tolower(key) == stop) {
	animacija = false;
    }
    
}

//postavljanje scene, osvjetljenja i sjencanja
void initScene() {

    // omogućivanje osvjetljenja
    glLightfv(GL_LIGHT0, GL_AMBIENT,  ambientLight);
    glLightfv(GL_LIGHT0, GL_POSITION, positionLight);

    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.01);

    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);

    glEnable(GL_COLOR_MATERIAL);

    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,  whiteSpecMate);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);

    glShadeModel(GL_SMOOTH);

}

//crtanje scene
void drawScene()
{   float x,y;
    x = getValue(dist, pt_x);
    y = getValue(dist, pt_y);

    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glClearColor (0.3, 0.3, 0.3, 0.0);
    glMatrixMode ( GL_MODELVIEW ); 	 // idemo u perspektivu
    glLoadIdentity();			 // resetiranje

    gluLookAt ( 2.6,1.0,9.8, 		// camera
                2.6,0.0,2.0, 		// where
                0.0f,1.0f,0.0f ); 	// up vector
    

    glPushMatrix();   
      
      //crtanje podloge
      drawGround();
    
      //inicijaliza scene
      initScene();
    
      // crtanje i postavljanje lopte, traga te krivulje
        glPushMatrix();
            glTranslatef(0,0,0);
            glPushMatrix();
	   
	    if(brojac <=400) 
	    {
		drawBall(30, 30,x,y);
	    }
	    else {
		drawBall(30, 30,6.8,0);
	    }
	    
	drawPath(x, y);
	
            glPopMatrix();
        //ukljucivanje ove sljedece 2 komande uzrokovat ce prikaz kontrolnih točaka te Bezierove krivulje
            //drawControlPoints(); 
            //drawBezierCurve();
        glPopMatrix();
    glPopMatrix();  
    glutSwapBuffers();
}


void update ( int /*value*/ )
{ 

if(animacija==true){
    t+=1;
    brojac++;
    dist = t/100;
}  
    glutPostRedisplay();
 
  //druga Bezierova linija
    if (brojac == 101)
    {
      pt_x << 0.9, 1.7, 2.6, 3.3;
      pt_y << 0, 2.2, 1.6, 0 ;
      pt_z << 0, 0, 0, 0;
      generateBezierCurve();
      t=0;
    }
  //treca Bezierova linija
    else if (brojac == 202)
    {
      pt_x << 3.3, 4.0, 4.8, 5.4;
      pt_y << 0, 1.3, 1, 0 ;
      pt_z << 0, 0, 0, 0;
      generateBezierCurve();
      t=0;
    
    }
  //cetvrta Bezierova linija
    else if (brojac == 303)
    {
      pt_x << 5.4, 5.9, 6.4, 6.8;
      pt_y << 0, 1, 0.5, 0 ;
      pt_z << 0, 0, 0, 0;
      generateBezierCurve();
      t=0;;
    }
  //animacija dolazi do kraja,napravljena su 4 odskakivanja/loopa te se lopta zaustavlja
    else if (brojac == 404)
    {
     animacija=false;
    }
   
   
   //update glut-a nakon 25 ms
    glutTimerFunc ( 25, update, 0 );

}



int main ( int argc, char **argv )
{	
  //unos koordinata za točke za prvu Bezierovu krivulju
    pt_x << -1.8, -0.8, -0.1, 0.9;
    pt_y << 2.6, 2.6, 2, 0 ;
    pt_z << 0, 0, 0, 0;
    dist = 0.;
  //stvaranje prve Bezierove krivulje
    generateBezierCurve();

  //inicijalizacija
    glutInit ( &argc, argv );
    
    glutInitDisplayMode ( GLUT_DOUBLE /*| GLUT_RGB | GLUT_DEPTH */);
    glutInitWindowSize ( 1000, 1000 );

  //kreiranje prozora (prikaz, tipke itd.)
    glutCreateWindow ( "Julian Cop, zadatak br.1.        tipke: S-start  X-stop  ESC-exit" );
    glutReshapeFunc ( changeSize );
    glutDisplayFunc ( drawScene );
    glutKeyboardFunc(tipke);
    glutTimerFunc ( 25, update, 0 );
    glShadeModel (GL_SMOOTH);
       glClearDepth(0.1f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    
    glutMainLoop();

    return 0;
}

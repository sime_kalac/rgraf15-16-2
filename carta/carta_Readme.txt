Zadaća iz računalne grafike, zadatak broj 1.

Upute za pokretanje:	

			- Tipka 's' pokreće animaciju
			
			- Tipka 'x' zaustavlja animaciju u bilo kojem trenutku
		
			- Tipka 'Esc' zatvara prozor animacije

Funkcija 'keyboard' služi samo za user input, sa navedenim mogućnostima, tj. tipkama.


Razvojna okolina:

			- KDevelop 4 na Ubuntu 14.10
			
			- korišteni su GLUT i GL paket


	Prilikom pokretanja nije potrebno unositi nikakve argumente. Nakon pokretanja animacije 

pritiskom na tipku 's' loptica kreće iz gornjeg desnog kuta. Njen smjer kretanja određen je 

unaprijed definiranom putanjom. Loptica za sobom ostavlja crveni trag. Animacija završava kada 

ona dođe u donji lijevi kut ili je zaustavljena tipkom 'x'.


	Veličina prozora OpenGL-a je određena na 800x600 te je prema tome napravljena točna putanja 

kako bi loptica imala početak i završetak u zadanim pozicijama.

Navedena putanja loptice napravljena je uz pomoć Bezierovih krivulja. One su realizirane sa više 

funkcija koje se mogu vidjeti u kodu.

	
	U main-u se vrši inicijalizacija GLUT-a te se stvara prozor. Iz nje se zovu sve potrebne funkcije 
	
kao i glutMainLoop() koja je sastavni dio svakog programa.


	Kao reference koristila sam primjere sa vježbi kao i svih materijala sa sustava Mudri. Gledala sam  

primjere sa OpenGL wiki te dokumentaciju na www.opengl.org. Također pomogli su razni tutorijali na 

Youtubu i stranice kao Stack Overflow, http://www.swiftless.com, http://www.glprogramming.com  i slično.





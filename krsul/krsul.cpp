#include <GL/gl.h>
#include <GL/glut.h>
#include <cmath>
#include<array>

#include <iostream>
#include <Eigen/Dense>
using namespace Eigen;
using namespace std;

//inicijalizacija kuteva za vršenje rotacija oba klatna
float angle1=0.f;
float angle2=0.f;
float angle3=0.f;
float angle4=0.f;
float angle5=0.f;
float angle6=0.f;

float radius=0.02f; //radijus kugli


bool animacija; //bool varijabla koja služi za pokretanje/pauziranje animacije

//variable kojima kontroliramo koja rotacija je uključena i za koje klatno
bool rotacija1Z;
bool rotacija1X;
bool rotacija1Y;
bool rotacija2Z;
bool rotacija2X;
bool rotacija2Y;

bool prvi=false;


int pointsCount = 0; //counter za spremanje koordinata točki kroz koje prolazi zadnja kugla (ona koja ostavlja crveni trag)
float points[100][16]; //u ovo polje spremaju se vrijednosti koje dobijemo funkcijom glGetFloatv()
		      //funkcija vraća 16 vrijednosti od kojih 12., 13. i 14. koristimo kako bi saznali koordinate točke




void update(int /*value*/) //ova funkcija poziva se svakih 25 ms, mijenjaju se vrijednosti kuteva za koje se rotiraju tijela ovisno o tome koju smo tipku pritisnuli
{		      
  if (animacija){
    if(rotacija1Z){
      angle3 +=1.0f;      
      if(angle3>360) angle3 -= 360;
    }
    
    if(rotacija1Y){
      angle2 +=1.0f;
      if(angle2>360) angle2 -= 360;
    }
    
    if(rotacija1X){
      angle1 +=1.0f;
      if(angle1>360) angle1 -= 360;
    }
  
     if(rotacija2Z){
      angle6 +=1.0f;
      if(angle6>360) angle6 -= 360;
    }
    
    if(rotacija2Y){
      angle5 +=1.0f;
      if(angle5>360) angle5 -= 360;
    }
    
    if(rotacija2X){      
      angle4 +=1.0f;
      if(angle4>360) angle4 -= 360;  
    }   
   }
   glutPostRedisplay();
   glutTimerFunc(25, update, 0);
}  
    



void handleKeypress (unsigned char key, int /*x*/, int /*y*/){ //funkcija koja pri pritisku odgovarajuće tipke mijenja odgovarajuće varijable
    
  switch(key){
    case 27: //ESC
      exit(0);
    case 's':{
      animacija=true;    
      break;
    }
    case 'x':{
      animacija=false;
      break;
    }
    
    case'e':{
      rotacija1Z=true;
      rotacija1X=false;
      rotacija1Y=false;
      break;
    }
    
    case'w':{
      rotacija1Z=false;
      rotacija1X=false;
      rotacija1Y=true;
      break;
    }
    
    case'q':{
      rotacija1Z=false;
      rotacija1X=true;
      rotacija1Y=false;
      break;
    }
    case 'm':{
      rotacija2Z=true;
      rotacija2X=false;
      rotacija2Y=false;
      break;
    }
      
    case 'n':{
      rotacija2Z=false;
      rotacija2X=false;
      rotacija2Y=true;
      break;
      
    }
    
    case 'b':{
      rotacija2Z=false;
      rotacija2X=true;
      rotacija2Y=false;
      break;      
    }      
  } 
}

void initRendering() {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING); //Enable lighting
	glEnable(GL_LIGHT0); //Enable light #0
	glEnable(GL_LIGHT1); //Enable light #1
	glEnable(GL_NORMALIZE); //Automatically normalize normals
	glShadeModel(GL_SMOOTH); //Enable smooth shading	
}


void handleResize (int w, int h){
    //tell openGl how to convert from coordinates to pixel values
  glViewport(0,0,w,h);
  
  glMatrixMode(GL_PROJECTION); //switch to setting the camera gluPerspective
  
  //set camera perspective
  glLoadIdentity(); //reset the camera
  gluPerspective(45.0, //camera angle
		(double)w / (double)h, //width-to-hight ratio
		 1.0, //the near z clipping coordinate
		 200.0); //the far z clipping coordinate
  
}

void crvenitrag() //prema točkama zapisanim u polju points iscrtavamo crveni trag
{
    glBegin(GL_LINE_STRIP); //koristimo GL_LINE_STRIP kako bi se točke polja povezale linijom
    glColor3f(1.0f,0.0f,0.0);
    if (!prvi){ //ako smo tek na početku animacije cijelo polje nam nije popunjeno, stoga ispisujemo samo do countera
    for(int j = 0; j < pointsCount; j++) glVertex3f(points[j][12], points[j][13], points[j][14]); 
    }
    else{ //kad smo već jednom ispunili polje
	for(int j = 0; j < 100; j++) glVertex3f(points[j][12], points[j][13], points[j][14]);
    }
    glEnd();
}


void drawScene()
{
    GLfloat ambientColor[] = {1.f, 1.f, 1.f, 1.0f}; //Color (0.2, 0.2, 0.2)
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);
    
    
  
    glClearColor (0.0,0.0,0.0,1.0); //crna pozadina
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    

    glLoadIdentity();
    
        
    GLUquadricObj *qobj; //varijabla koju smo morali stvoriti zbog crtanja cilindara
    qobj = gluNewQuadric();
       
    glPushMatrix();
   
    
    glTranslatef(0.8f, -0.4f, 0.0f); //pomicanje svega u središte
    glScalef(0.9,0.9,0.9); //prilogađavanje veličina

      //rotacija prvog klatna oko Z
      glTranslatef(-0.8f, 0.5f,0.f);
      glRotatef(angle3, 0.f, 0.f, 1.f );  
      glTranslatef(0.8f, -0.5f,0.f);
      
      //rotacija prvog klatna oko Y
      glTranslatef(-0.8f, 0.5f,0.f);
      glRotatef(angle2, 0.f, 1.f, 0.f );  
      glTranslatef(0.8f, -0.5f,0.f);
      
      //rotacija prvog klatna oko X
      glTranslatef(-0.8f, 0.5f,0.f);
      glRotatef(angle1, 1.f, 0.f, 0.f );  
      glTranslatef(0.8f, -0.5f,0.f);
      
      glColor3f(1.0f, 1.0f, 0.0f); //žuta boja
      
      //crtanje prve kugle
      glPushMatrix();
	glTranslatef(-0.8f, 0.5f,0.f);
	glutSolidSphere(radius, 100,100); 
      glPopMatrix(); 
          
      glColor3f(1.0f, 0.0f, 0.0f); //crvena boja
      
      //crtanje cilindra prvog klatna
      glPushMatrix();
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.f, -0.8f,-0.48f);
	gluCylinder(qobj,0.02f,0.02f,0.46f,100,100);
      glPopMatrix();
      
      glPushMatrix();
	//rotacija drugog klatna oko Z
	glTranslatef(-0.8f, 0.f,0.f);
	glRotatef(angle6, 0.f, 0.f, 1.f );  
	glTranslatef(0.8f, 0.f,0.f);
      
	//rotacija drugog klatna oko Y
	glTranslatef(-0.8f, 0.f,0.f);
	glRotatef(angle5, 0.f, 1.f, 0.f );  
	glTranslatef(0.8f, 0.f,0.f);
	
	//rotacija drugog klatna oko X
	glTranslatef(-0.8f, 0.f,0.f);
	glRotatef(angle4, 1.f, 0.f, 0.f );  
	glTranslatef(0.8f, 0.f,0.f);
      
      
	glColor3f(1.0f, 1.0f, 0.0f); //žuta boja
	
	//crtanje druge kugle
	glPushMatrix();
	  glTranslatef(-0.8f,0.f,0.f);
	  glutSolidSphere(radius, 100,100);
	glPopMatrix(); 
	
	
	glColor3f(1.0f, 0.0f, 0.0f); //crvena boja
	
	//cratnje cilindra drugog klatna
	glPushMatrix();
	  glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	  glTranslatef(0.f, 0.f,-0.78f);
	  gluCylinder(qobj,0.02f,0.02f,0.46f,100,100);
	glPopMatrix();
	
	glColor3f(1.0f, 1.0f, 0.5f); //boja zadnje kugle
	
	//crtanje zadnje kugle
	glPushMatrix();
	  glTranslatef(-0.305f,0.f,0.f);
	  glPushMatrix();
	    glutSolidSphere(radius, 100,100);
	    if(animacija){ //ako je animacija pokrenuta onda se dobivaju nove koordinate zadnje kugle
	    
	    if (pointsCount==100){ //ako smo popunili polje counter stavljamo na 0
	      pointsCount=0;
	      prvi=true;	//javljamo da smo prvi put popunili polje zbog ispisa crvenog traga      
	    }
            glGetFloatv(GL_MODELVIEW_MATRIX, &points[pointsCount][0]); //dobivanje 16 vrijednosti među kojima se nalaze kooridane zadnje kugle
	    pointsCount++;  //povećavanje brojača
	    }
	    
	  glPopMatrix();
	glPopMatrix(); 
      glPopMatrix();	         
    crvenitrag();      
    glutSwapBuffers(); 
}

//podešavanje potrebih varijabli za osvjetljenje
const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { -2.0f, 0.5f, 1.0f, 0.0f };

const GLfloat mat_ambient[]    = { 0.2f, 0.2f, 0.2f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.f, 0.f, 0.f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };

int main(int argc, char **argv)
{
     
    glutInit(&argc, argv); 
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(1000, 1000);

    glutCreateWindow("v3 - p1");
    
    glutDisplayFunc(drawScene);
    glutKeyboardFunc(handleKeypress);
    glutTimerFunc(25, update, 0);
    
   update(0);
    glEnable(GL_LIGHT0);//ukljuci svjetlinu
    glEnable(GL_NORMALIZE);//ukljucuje automatsku normalizaciju vektora normala
    glEnable(GL_COLOR_MATERIAL);//boja materijala
    glEnable(GL_LIGHTING);//ukljucuje osvjetljenje

    //izvor svjetlosti s parametrima:
    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);//ambijentalna komponenta svjetlosti
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);//difuzna 
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);//odsjaj
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);//pozicija

     //materijal
    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);
    
    
    //upute za korištenje
    printf(" ESC - exit, s - pokretanje animacije, x - pauza, q - rotacija 1. klatna oko X, w - rotacija 1. klatna oko Y, e - rotacija 1. klana oko Z,\n b - rotacija 2. klatna oko X , n - rotacija 2. klatna oko Y, m - rotacija 3. klatna oko Z.\n");
   
    glutMainLoop();

    return 0;
}

#include <GL/gl.h>
#include <GL/glut.h>
#include <cmath>
#include<math.h>

#include <iostream>
#include <Eigen/Dense>

using namespace Eigen;
using namespace std;

long binomials ( long n, long k )
{
    // n && k >=0
    long i;
    long b;

    if ( 0 == k || n == k )
    {
        return 1;
    }

    if ( k > n )
    {
        return 0;
    }

    if ( k > ( n - k ) )
    {
        k = n - k;
    }

    if ( 1 == k )
    {
        return n;
    }

    b = 1;

    for ( i = 1; i <= k; ++i )
    {
        b *= ( n - ( k - i ) );

        if ( b < 0 ) return -1; /* ERR!!!! OVERFLOW */

        b /= i;
    }

    return b;
}

double polyterm ( const int &n, const int &k, const double &t )
{
    return pow ( ( 1.-t ),n-k ) *pow ( t,k );
}

double getValue ( const double &t, const VectorXd &v )
{
    int order = v.size()-1;
    double value = 0;

    for ( int n=order, k=0; k<=n; k++ )
    {
        if ( v ( k ) ==0 ) continue;

        value += binomials ( n,k ) * polyterm ( n,k,t ) * v ( k );
    }

    return value;
}

void changeSize ( int w, int h )
{
    if ( h == 0 )
    {
        h = 1;
    }

    float ratio = 1.0* w / h;

    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    glViewport ( 0, 0, w, h );
    gluPerspective ( 45,ratio,1,1000 ); // view angle u y, aspect, near, far
}

Vector4d pt_x, pt_y, pt_z;
VectorXd t_x, t_y, t_z;
VectorXd ts;
Vector3d bezier_control_points_coo;

//drawing bezier curve
void drawControlPoints()
{
    glPushMatrix();
    glColor3f ( 1.f, 1.f, 1.f );
    glBegin ( GL_LINE_STRIP );
    glVertex3f ( pt_x ( 0 ), pt_y ( 0 ), pt_z ( 0 ) );
    glVertex3f ( pt_x ( 1 ), pt_y ( 1 ), pt_z ( 1 ) );
    glVertex3f ( pt_x ( 2 ), pt_y ( 2 ), pt_z ( 2 ) );
    glVertex3f ( pt_x ( 3 ), pt_y ( 3 ), pt_z ( 3 ) );
    glEnd();
    glPopMatrix();
}
/*
void generateBezierCurve()
{
    ts = VectorXd::LinSpaced ( 15,0,1. );
    t_x.resize ( ts.size() );
    t_y.resize ( ts.size() );
    t_z.resize ( ts.size() );

    for ( int idx=0; idx< ts.size(); ++idx )
    {
        t_x ( idx ) = getValue ( ts ( idx ), pt_x );
        t_y ( idx ) = getValue ( ts ( idx ), pt_y );
        t_z ( idx ) = getValue ( ts ( idx ), pt_z );
        }
}
*/
// draw i kreira tocke i crta, trebalo bi raditi jednu stvar
void drawBezierCurve()
{
    glPushMatrix();
    glColor3f ( 0.1f, 0.1f, 0.1f );
    //glBegin ( GL_LINE_STRIP );//za 1. zad
    for ( int i=0; i< ts.size(); ++i )
    {
        glVertex3f ( t_x ( i ), t_y ( i ), t_z ( i ) );
    }
    glEnd();
    glPopMatrix();
}

Vector3d sphere_coo;
//crtanje kugle
void drawSphere()
{
    glColor3d(0.2,0.2,0.2);

    glPushMatrix();
        glTranslatef(sphere_coo(0), sphere_coo(1), sphere_coo(2));
        glutSolidSphere(0.5, 15,15);
    glPopMatrix();
}

const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { 2.0f, 5.0f, 5.0f, 0.0f };

const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };

float angle_0=-90;
float angle_n=0;

float angle = -90;

void drawScene()
{
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glMatrixMode ( GL_MODELVIEW ); // idemo u perspektivu
    glLoadIdentity(); // resetiranje

    gluLookAt ( 0.0,0.0,10.0, // camera
                           0.0,0.0,-1.0, // where
                           0.0f,1.0f,0.0f ); // up vector

    glPushMatrix();
        glRotatef(angle, 0, 1, 0);
        glPushMatrix();
            glTranslatef(-pt_x(0), -pt_y(0), -pt_z(0));
           drawControlPoints();
           drawBezierCurve();
           drawSphere();
        glPopMatrix();
    glPopMatrix();

    glutSwapBuffers();
}

bool case_e;

void update ( int /*value*/ )
    {
        glutPostRedisplay();
        if(angle<0)
        {
            double t = (angle - angle_0)/(angle_n - angle_0);
            sphere_coo(0) = getValue(t, pt_x);
            sphere_coo(1) = getValue(t, pt_y);
            sphere_coo(2) = getValue(t, pt_z);
        } else {
            // za animaciju povratak ovdje staviti counter
            // ne radi  = 0 jer je translatiran koo sustav
            sphere_coo(0) = pt_x(0);
            sphere_coo(1) = pt_y(0);
            sphere_coo(2) = pt_z(0);
        }

        if (angle > 0){angle=0;}
        if(case_e == true) angle+=1;
        //update glut-a nakon 25 ms
        glutTimerFunc ( 90, update, 0 );
    }

float x_t1, x_t2, x_t3, x_t4;
float y_t1, y_t2, y_t3, y_t4;
float z_t1, z_t2, z_t3, z_t4;

//keyboard event handler
void SpecialInput(int key, int , int )
{
    float t = 0.1;
    switch(key)
    {
        case GLUT_KEY_LEFT:
            y_t2-=t;
            y_t3-=t;
            pt_x << x_t1, x_t2, x_t3, x_t4;
            pt_y << y_t1, y_t2, y_t3, y_t4;
            pt_z << z_t1, z_t2, z_t3, z_t4;

        break;
        case GLUT_KEY_RIGHT:
            y_t2+=t;
            y_t3+=t;
            pt_x << x_t1, x_t2, x_t3, x_t4;
            pt_y << y_t1, y_t2, y_t3, y_t4;
            pt_z << z_t1, z_t2, z_t3, z_t4;
            break;
      }
    glutPostRedisplay();
}

void keyboard(unsigned char key, int , int )
{
        case_e = true;
        switch (key)
        {
            case 27: // Escape key
                exit(0);
                break;
            case 's'://start animation
                glutDisplayFunc ( drawScene);
                break;
            case 'e'://pause animation
                case_e = false;
                break;
        }
}

/*static void resize(int width, int height)
{
    const float ar = (float) width / (float) height;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-ar, ar, -1.0, 1.0, 2.0, 100.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
}
*/
static void display(void)
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glColor3d(1,0,0);

    glPushMatrix();
        glTranslated(0.0,1.2,-6);
        glutSolidSphere(1,50,50);
    glPopMatrix();

    glutSwapBuffers();
}

int main ( int argc, char **argv )
{
    x_t1=0.5; x_t2=-0.25; x_t3=0.6; x_t4=-0.55;
    y_t1=-0.5, y_t2=0.5, y_t3=0.6, y_t4=-0.5;
    z_t1=0, z_t2=2, z_t3=2, z_t4=4;

    pt_x << x_t1, x_t2, x_t3, x_t4;
    pt_y << y_t1, y_t2, y_t3, y_t4;
    pt_z << z_t1, z_t2, z_t3, z_t4;
    angle = angle_0;
    //generateBezierCurve();

    sphere_coo << pt_x(0), pt_y(0), pt_z(0);
    glutInit ( &argc, argv );
    glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize ( 700, 700 );

    glutCreateWindow ( "projekt_zad_4" );
    glutReshapeFunc ( changeSize);

    glutDisplayFunc(display);
        glClearColor(0,0,0,0);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);

        glEnable(GL_LIGHT0);
        glEnable(GL_NORMALIZE);
        glEnable(GL_COLOR_MATERIAL);
        glEnable(GL_LIGHTING);

        glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
        glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
        glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
        glLightfv(GL_LIGHT0, GL_POSITION, light_position);

        glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
        glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);

    glutKeyboardFunc(keyboard);
    glutSpecialFunc(SpecialInput);

    glutTimerFunc ( 25, update, 0 );
    glutMainLoop();

    return 0;
}
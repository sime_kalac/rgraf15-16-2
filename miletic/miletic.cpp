#include <GL/gl.h>
#include <GL/glut.h>
#include <cmath>
#include<array>
#include <iostream>
#include <Eigen/Dense>
using namespace Eigen;
using namespace std;
void lightening(){
      GLfloat mat_ambient[]={0.3,0.3,0.3,1.0};
      GLfloat mat_diffuse[]={0.6,0.6,0.6,1.0};
      GLfloat mat_specular[]={0.9,0.9,0.9,1.0};
      GLfloat mat_shininess[]={100.0};
  
      glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,mat_ambient);
      glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,mat_diffuse);
      glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,mat_specular);
      glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,mat_shininess);
  
      GLfloat light0_ambient[]={0.5,0.5,0.5,1.0};
      GLfloat light0_diffuse[]={1.0,1.0,1.0,1.0};
      GLfloat light0_specular[]={1.0,1.0,1.0,1.0};
      GLfloat light0_position[]={5.0,5.0,2.0,0.0};
  
      glLightfv(GL_LIGHT0,GL_AMBIENT,light0_ambient);
      glLightfv(GL_LIGHT0,GL_DIFFUSE,light0_diffuse);
      glLightfv(GL_LIGHT0,GL_SPECULAR,light0_specular);
      glLightfv(GL_LIGHT0,GL_POSITION,light0_position);
      glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE,light0_ambient);
  
      glEnable(GL_LIGHTING);
      glEnable(GL_LIGHT0);
      glEnable(GL_DEPTH_TEST);
      glShadeModel(GL_SMOOTH);
      glEnable(GL_COLOR_MATERIAL);
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
bool start = false;
bool rot_Z1 = false;
bool rot_X1 = false;
bool rot_Y1 = false;
bool rot_Z2 = false;
bool rot_X2 = false;
bool rot_Y2 = false;
float kut1=0.f;
float kut2=0.f;
float kut3=0.f;
float kut4=0.f;
float kut5=0.f;
float kut6=0.f;
double radius=0.08f;
float array1[150][16] = {0};
int counter = 0;
bool flag = false;

void update_angle(int /*value*/) 
{  
  if (start){
    if(rot_Z1){ kut1 +=2.0f;      
		if(kut1>360) kut1 = 0;}
    if(rot_Y1){kut2 +=2.0f;
		if(kut2>360) kut2 = 0;}
    if(rot_X1){kut3 +=2.0f;
		if(kut3>360) kut3 = 0;}
    if(rot_Z2){kut4 +=2.0f;
		if(kut4>360) kut4 = 0;}
    if(rot_Y2){kut5 +=2.0f;
		if(kut5>360) kut5 = 0;}
    if(rot_X2){kut6 +=2.0f;
		if(kut6>360) kut6 = 0;}   
    }
   glutPostRedisplay();
   glutTimerFunc(30, update_angle, 0); 
}  
void keyboard (unsigned char key, int/* x*/, int /*y*/){
	  if(key == 27) exit(0);
	  if(key == 's') {
		  start = true;
		  update_angle(0);
	  }
	  if(key == 'x') {
		  start = false;
		  flag = true;
		  update_angle(0);
	  }
	  if(key == 'e'){
	      rot_Z1=false;
	      rot_X1=true;
	      rot_Y1=false;
	      flag = true;
	      update_angle(0);
	  }
	  if(key == 'w'){
	      rot_Z1=false;
	      rot_X1=false;
	      rot_Y1=true;
	      flag = true;
	      update_angle(0);
	  }
	  if(key == 'q'){
	      rot_Z1=true;
	      rot_X1=false;
	      rot_Y1=false;
	      flag = true;
	      update_angle(0);
	  }
	  if(key == 'b'){
	      rot_Z2=true;
	      rot_X2=false;
	      rot_Y2=false;
	      flag = true;
	      update_angle(0);
	  }
	  if(key == 'm'){
	      rot_Z2=false;
	      rot_X2=true;
	      rot_Y2=false;
	      flag = true;
	      update_angle(0);
	  }
	  if(key == 'n'){
	      rot_Z2=false;
	      rot_X2=false;
	      rot_Y2=true;
	      flag = true;
	      update_angle(0);
	  }
}
void drawLine(){
    glBegin(GL_LINE_STRIP);
    glColor3f(1.0f,0.0f,0.0);
    
    for(int i = 0; i < counter; i++){

        glVertex3f(array1[i][12], array1[i][13], array1[i][14]);
    }
    glEnd();
}
void sphere_line(){
	glColor3f(1.0f, 1.0f, 0.0f);
	glPushMatrix();
	 glTranslatef(-0.405f,0.f,0.0f);
	  glPushMatrix();
	    glutSolidSphere(radius, 150,150);
	    if(start == true && flag == true){
	      
	      if(counter == 150) counter = 0;
	      glGetFloatv(GL_MODELVIEW_MATRIX, &array1[counter][0]);
	      counter++;
	    }
	  glPopMatrix();
	glPopMatrix();  
}
void rotate1(){
    GLUquadricObj *object;
    object = gluNewQuadric();
    glTranslatef(0.8f, -0.3f, 0.0f); 
	    glTranslatef(-0.8f, 0.5f,0.f); 
	    glRotatef(kut1, 0.f, 0.f, 1.f );  
	    glTranslatef(0.8f, -0.5f,0.f);
	    
	    glTranslatef(-0.8f, 0.5f,0.f);
	    glRotatef(kut2, 0.f, 1.f, 0.f );  
	    glTranslatef(0.8f, -0.5f,0.f);
	    
	    glTranslatef(-0.8f, 0.5f,0.f);
	    glRotatef(kut3, 1.f, 0.f, 0.f );  
	    glTranslatef(0.8f, -0.5f,0.f);
	
	    glColor3f(1.0f, 1.0f, 0.0f);
		glPushMatrix();
		  glTranslatef(-0.8f, 0.5f,0.f);
		  glutSolidSphere(radius, 150,150);
		glPopMatrix(); 
		
	    glColor3f(0.0f, 1.0f, 0.0f);
		glPushMatrix();
		  glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		  glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		  glTranslatef(0.f, -0.8f,-0.48f);
		  gluCylinder(object,0.04f,0.04f,0.46f,100,100);
		glPopMatrix();
  
}
void rotate2(){
    GLUquadricObj *object;
    object = gluNewQuadric();
  	glPushMatrix();
		glTranslatef(-0.8f, 0.f,0.f);
		glRotatef(kut4, 0.f, 0.f, 1.f );  
		glTranslatef(0.8f, 0.f,0.f);
	      
		glTranslatef(-0.8f, 0.f,0.f);
		glRotatef(kut5, 0.f, 1.f, 0.f );  
		glTranslatef(0.8f, 0.f,0.f);
		
		glTranslatef(-0.8f, 0.f,0.f);
		glRotatef(kut6, 1.f, 0.f, 0.f );  
		glTranslatef(0.8f, 0.f,0.f);
	      
		glColor3f(1.0f, 1.0f, 0.0f);
		glPushMatrix();
		  glTranslatef(-0.8f,0.f,0.f);
		  glutSolidSphere(radius, 150,150);
		glPopMatrix(); 
		
		glColor3f(0.0f, 1.0f, 0.0f);
		glPushMatrix();
		  glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		  glTranslatef(0.f, 0.f,-0.78f);
		  gluCylinder(object,0.04f,0.04f,0.36f,100,100);
		glPopMatrix();
		
		sphere_line();
		
	glPopMatrix();
	  
}
void drawFigure(){
      glPushMatrix(); 
	  glColor3f(0.0f, 1.0f, 0.0f);
	  glPushMatrix();
	  glTranslatef(-0.0f, 0.3825f, -2.0f); 
	  glutSolidCube (0.2f);
	  glPopMatrix();
	  rotate1();
	  rotate2();
      glPopMatrix(); 
}
void myScene(){
    glClearColor (1.0,1.0,1.0,1.0);
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    gluLookAt (2.0, 2.0, 2.0, 1.5, -3.0, 0.0, 0.0, 1.0, 0.0); 
    glLoadIdentity();
    drawFigure();
    drawLine();
    glutSwapBuffers(); 
}
void reshape(int w,int h){
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if(w<=h) glOrtho(-2.0,2.0,-2.0*(GLfloat)h/(GLfloat)w,2.0*(GLfloat)h/(GLfloat)w,-10.0,10.0);
    else glOrtho(-2.0*(GLfloat)h/(GLfloat)w,2.0*(GLfloat)h/(GLfloat)w,-2.0,2.0,-10.0,10.0);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}
int main(int argc, char **argv){
    glutInit(&argc, argv); 
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(700, 700);
    glutInitWindowPosition(30,30);
    glutCreateWindow("zadatak3 - Matija_Miletic");
    glutReshapeFunc(reshape); 
    glutDisplayFunc(myScene);
    glutKeyboardFunc(keyboard);
    glutTimerFunc(30, update_angle, 0);
    update_angle(0); 
    lightening();
    glutMainLoop();
    return 0;
}

Zadatak 4.


Varijable:
diffuseMaterial: proizvoljna siva boja sfere
ostale boje su sve bijele
shinines je proizvoljno odabran
beziertoc predstavlja tocke bezierove krivulje koje ce se koristiti
index i vertices sluze za kreiranje sfere
rotation - definira rotaciju
tval - definira oblik krivulje
increment - definira brzinu kretanja sfere po krivulji


Nema argumenata za pokretanje.


Sfera se dobije višestrukim pozivima rekurzivne funkcije koja dijeli 20 jednakih trokuta na manje dijelove dok se ne dobije oblik istovjetan sferi. Konkretna dubina do koje se dijele u programu je 3 poziva (promjenivo u 'split' funkciji).
Bezier krivulja oboja se i iscrta se u 3d prostoru (ovisno o beziertoc matrici).


Navigacija i kontrola:
s - pokretanje/nastavljanje animacije
e - pauziranje animacije
ESC - izlazak/prekid programa
d - rotacija krivulje lijevo
g - rotacija krivulje desno
r - 10% vece skaliranje krivulje
f - 10% manje skaliranje krivulje


Reference:
laboratorijske vjezbe
www.stackoverflow.com - vise primjera
www.opengl.org - vise primjera
www.cplusplus.com
www.tutorialspoint.com
www.glprogramming.com
https://en.wikipedia.org/wiki/File:Loop_Subdivision_Icosahedron.svg
http://www.gamedev.net/topic/274154-icosahedron-recursive-subdivision/

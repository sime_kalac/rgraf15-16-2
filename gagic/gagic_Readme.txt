     _          _                  ____             _                  _   ____  
    / \   _ __ | |_ ___  _ __     / ___| __ _  __ _(_) ___   __   __  / | |___ \ 
   / _ \ | '_ \| __/ _ \| '_ \   | |  _ / _` |/ _` | |/ __|  \ \ / /  | |   __) |
  / ___ \| | | | || (_) | | | |  | |_| | (_| | (_| | | (__    \ V /   | |_ / __/ 
 /_/   \_\_| |_|\__\___/|_| |_|   \____|\__,_|\__, |_|\___|    \_/    |_(_)_____|
                                              |___/                              
											  
	ANIMACIJA ODSKAKIVANJA LOPTE
	
	Program je napravljen u Kdevelopu. Radna verzija 1.2- 25. siječnja 2016.
	
	UPUTE
	
	Za pokretanje se koristi tipka s, zaustavljanje X.
	Izlaz pomoću tipke ESC.
	
	OPIS
	
	Sfera se počinje kretati i odskakuje za sobom ostavljajući crveni trag. Kreće se Bezierovim krivuljama.
	Postoje određeni problemi u prikazu i zaustavljanju lopte koje nažalost u nekoliko verzija kroz tjedne nisam uspio riješiti.
	
	
	IZRADIO
	
	Anton Gagić
	
	IZVORI
	http://stackoverflow.com/questions/4350591/curve-trajectory-object-orientation
	http://stackoverflow.com/questions/785097/how-do-i-implement-a-b%C3%A9zier-curve-in-c
	https://en.wikipedia.org/wiki/B%C3%A9zier_curve
	http://www.dreamincode.net/forums/topic/320703-bezier-curves-part-1-linear-algebra-series/ (ideja za dijelove koda)
	
	
	
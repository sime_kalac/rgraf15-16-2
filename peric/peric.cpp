#include <iostream>
#include <GL/glut.h>

using namespace std;




// VARIABLES
    int osX1 = 0;
    int osY1 = 1;
    int osZ1 = 0;

    int osX2 = 0;
    int osY2 = 1;
    int osZ2 = 0;

    // inicijalno ne želimo da animacija ide, stoga joj dodjelimo vrijednost "false"
    bool animationPlaying = false;


    // animated variables
    double animProgress = 1.0; // pozcija klatila
    double animSpeed = 1.0; // brzina klačenja
    double animProgressMax = 64; // maksimalni kut klatila
    bool backwords = false; // ide li klatilo unatrag




// FUNCTIONS
    void key(unsigned char key, int x, int y) // keys and mouse position (x,y)
    {
        switch (key)
        {
            // izlaz iz programa
            case 27: exit(0); break;

            // kontrole valjka 1
            case 'q':
                osX1 = 1;
                osY1 = 0;
                osZ1 = 0;
                break;

            case 'w':
                osX1 = 0;
                osY1 = 1;
                osZ1 = 0;
                break;

            case 'e':
                osX1 = 0;
                osY1 = 0;
                osZ1 = 1;
                break;

            // kontrole valjka 2
            case 'b':
                osX2 = 1;
                osY2 = 0;
                osZ2 = 0;
                break;

            case 'n':
                osX2 = 0;
                osY2 = 1;
                osZ2 = 0;
                break;

            case 'm':
                osX2 = 0;
                osY2 = 0;
                osZ2 = 1;
                break;

            // kontole animacije
            case 's':
                animationPlaying = true;
                break;

            case 'x':
                animationPlaying = false;
                break;
        }

        glutPostRedisplay();
    }




// 3D OBJECTS
    void drawSphere(float size = 0.2f, int resolution = 16) {
        gluSphere(gluNewQuadric(), size, resolution, resolution);
    }

    void drawCylinder(float thickness = 0.2f, float height = 2.6f, int resolution = 16) {
        gluCylinder(gluNewQuadric(), thickness, thickness, height, resolution, resolution);
        glTranslatef(0, 0, height); // udalji transformaciju za visinu samog valjka, kako bi dalje nadodali kuglu
    }




// OPENGL/GLUT
    // zadrži proporcije prilikom suzivanja/ražiravanja samog aplikacijskog prozora
    void changeSize(int windowWidth, int windowHeight) {

        // visina prozora uvjek treba biti veća od 0, kako bi daljnje kalkulacije bile točne
        if(windowHeight == 0) {
            windowHeight = 1;
        }
        float ratio = 1.0 * windowWidth / windowHeight;

        // promijena moda matrice (na 2D ekran)
        glMatrixMode(GL_PROJECTION);

        // resetiranje transformacija matrice
        glLoadIdentity();

        // postavi viewport iste visine/širine kao i aplikacijski prozor
        glViewport(0, 0, windowWidth, windowHeight);

        // postavi ispravnu perspektivu
        gluPerspective(45, ratio, 1, 1000);

        // promijena moda matrice
        glMatrixMode(GL_MODELVIEW);
    }

    // cijelokupna openGL scena
    void renderScene(void)
    {
        // počisti boju i depth buffer (kako bi napravili mjesto za novu iteraciju slike)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // inicjalno dodjeli boju svim objektima (bijela u ovom slucaju)
        glColor3f(1, 1, 1);

        // resetiraj transformacije matrice
        glLoadIdentity();

        // postavi kameru
        gluLookAt(
            0.0f, 0.0f, 12.0f,
            0.0f, 0.0f,  0.0f,
            0.0f, 1.0f,  0.0f
        );


        // ukljući mogučnosti rendera
            glEnable(GL_DEPTH_TEST); // prikaži samo poligone koje su licem okrenuti kameri/viewport-u
            // (inaće bi stražnja i prednja strana poligina bila prikazana u isto vrijeme
            // (npr. prednja strana kocke i straža bi se vidjela u isto vrijeme))

            // svijetla
            glEnable(GL_LIGHTING); // omogući kalkulacije svijetla
            glEnable(GL_LIGHT0); // omogući "light0"

            glEnable(GL_NORMALIZE); // ispravljanje potencijalno krivih kalkulacija svijetla, zbog povečavanja/smanjivanja modela
            glEnable(GL_COLOR_MATERIAL); // omogući materijale



        // animacija (prilikom svake render iteracije, provjeravaju se uvijeti i prema tome ostvaruje željeni cilj)
            // animirano klatilo
            if(animationPlaying){
                if(animProgress <= animProgressMax && backwords == false){
                    animProgress += animSpeed;

                    if(animProgress == animProgressMax){
                        backwords = true;
                    }
                }

                if(backwords == true) {
                    animProgress -= animSpeed;

                    if(animProgress <= -animProgressMax){
                        backwords = false;
                    }
                }
            }


        // mesh colors - boje spremamo odvojeno u svoju varijablu, kako bi ju lakše dalje referencirali
        GLfloat meshColorGreen[] = { 0.33, 0.53, 0.33 };
        GLfloat meshColorBlue[] = { 0.26, 0.44, 0.66 };
        GLfloat meshColorRed[] = { 0.53, 0.0, 0.0 };
        GLfloat meshColorOrange[] = { 0.60, 0.40, 0.26 };


        // draw objects
            glPushMatrix();
                // origin
                glTranslatef(0, 3, 0); // pozicija kreiranja kugle (a i lančano sveg ostalog)
                glRotatef(90, 1, 0, 0); // zarotiraj 90° u X osi

                    // sphere #1
                    glRotatef(animProgress, osX1, osY1, osZ1); // -/+ prema Z osi
                    glColor3fv(meshColorGreen);
                    drawSphere();

                        // cylinder #1
                        glTranslatef(0, 0, .2); // udalji za poluomjer kugle#1
                        glColor3fv(meshColorBlue);
                        drawCylinder();
                        glTranslatef(0, 0, .2); // udalji za poluomjer kugle#2

                            // sphere #2
                            glRotatef(animProgress / 1.5, osX2, osY2, osZ2); // djelimo sa 1.5 kako bi drugi valjak išao sporije
                            glColor3fv(meshColorGreen);
                            drawSphere();

                                // cylinder #2
                                glTranslatef(0, 0, .2); // udalji za promjer kugle#2
                                glColor3fv(meshColorRed);
                                drawCylinder();
                                glTranslatef(0, 0, .2); // udalji za promjer kugle#3

                                    // sphere #3
                                    glColor3fv(meshColorOrange);
                                    drawSphere();
            glPopMatrix();



        // lights and materials
            // lights
                GLfloat lightAmbient[] = { 0.2f, 0.3f, 0.4f, 0.0f }; // dark-teal/blue
                GLfloat lightDiffuse[] = { 1.5f, 1.2f, 0.9f, 0.0f }; // medium-orange
                GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // white
                GLfloat lightPosition[] = { 10.0f, 10.0f, 5.0f, .0f }; // position

                // light0
                glLightfv(GL_LIGHT0, GL_POSITION, lightPosition); // pozicija svijetla
                glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient); // svijetlo od svih strana
                glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse); // boja svijetla
                glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular); // odsjaj svijetla

            // materials
                GLfloat materialSpecularColor[] = { 0.5f, 0.5f, 0.5f, 1.0f }; // white
                GLfloat materialShininess[] = { 32 }; // matte vs gloss (0-128)

                glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecularColor);
                glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);


        // double buffering (prikaži prednji buffer dok procesira stražnji)
        glutSwapBuffers();
    }




// APP
int main(int argc, char *argv[])
{
    // init GLUT
    glutInit(&argc, argv);


    // kreiraj aplikacijski prozor
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(800, 600);
    glutCreateWindow("OpenGL/GLUT - Klatilo 0.8 (Josip Peric 2016)");


    // poveži tipke
    glutKeyboardFunc(key);


    // callbacks
        // renderiraj cijelu scenu
        glutDisplayFunc(renderScene);
        // održi ispravne proprcije scene
        glutReshapeFunc(changeSize);


    // funkcija koja se izvodi svaki put dok je aplikacija u mirovanju (idle state), u ovom slučaju izvodi se naša scena (renderScene)
    glutIdleFunc(renderScene);


    // enter GLUT event processing cycle (it's a infinitive loop)
    glutMainLoop();


    return 0;
}

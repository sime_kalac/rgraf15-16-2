/* ****************** autor  ****************** */

Josip Perić 2016


/* ****************** reference ****************** */

    general manual:
        https://www.opengl.org/sdk/docs/man2/xhtml/gluCylinder.xml
        https://www.opengl.org/sdk/docs/man2/xhtml/gluSphere.xml
        https://www.opengl.org/sdk/docs/man2/xhtml/glTranslate.xml
        https://www.opengl.org/sdk/docs/man2/xhtml/glRotate.xml

    glLoadIdentity():
        http://stackoverflow.com/questions/628796/what-does-glloadidentity-do-in-opengl

    glMatrixMode():
        http://stackoverflow.com/questions/10408113/difference-between-glmatrixmodegl-projection-and-glmatrixmodegl-modelview

    hijearhisko modeliranje:
        http://web.cse.ohio-state.edu/~whmin/courses/cse581-2012-spring/transformation_h.pdf
        https://www.cs.utexas.edu/~fussell/courses/cs384g/lectures/lecture13-Hierarchical_modeling.pdf
        http://courses.cs.washington.edu/courses/cse457/01sp/lectures/05-hierarchical-modeling.pdf
        http://goanna.cs.rmit.edu.au/~gl/teaching/Interactive3D/2015/hierarchical-modelling.html
        http://3dopenglgraphics.blogspot.hr/2014/03/opengl-hierarchical-models.html
        https://www.cs.csustan.edu/~rsc/SDSU/Modeling.GLU.GLUT.pdf


/* ****************** kratki opis  ****************** */

Klatilo se klati po predefiniranom kutu. Drugi dio klatila se klati pod manjim kutem, kako bi se prividno dobio
prirodniji dojam klačenja.

Induvidualni objekti (kugla i valjak) su zasebno spremljeni u vlastitu funkciju, kako bi se kasnije mogli
referencirati u željenom hijararhiskom modeliranju.
(pogodnost odjeljivanja induvidualnih objekata je u tome da na jednom mjestu možemo promijeniti njihova svojstva, ne
utječući na već riješenu kompozicijsku cijelinu (u ovom slučaju sveukupno-klatilo))

kontole klatila:
    s - pokreni animaciju
    x - zaustavi animaciju
    esc - izađi iz programa
    q, w, e - promjena osi prvog zgloba
    b, n, m - promjena osi drugog zgloba

neke od varijabli:
    double animProgress = 1.0; // pozcija klatila
    double animSpeed = 1.0; // brzina klačenja
    double animProgressMax = 64; // maksimalni kut klatila

Varijable su ciljanje da budu što smislenije, da jasno samu sebe opisuju.
(za razliku od nečeg kriptiranog "xTtZA_t_ka", potencijalno čovjeku nekoristivno)

Cijelokupna konstrukcija samog klatila je dobivena korišteći openGL funkcije glTranslatef() i glRotatef(), koje
manipuliraju trenutačnom matricom.
Izmijenom vrijednosti u bilo kojem koraku kreiranja klatila, mijenjamo i njegov izgled te krajnje i animaciju.
(npr. glRotatef(kut, x, y, z) možemo mijenjati tako što na mjesto argumenta "kut" dodijelimo promijenjivu varijablu,
koja će rezultirati i promijenu u samoj matrici, te krajnje i željeni pomak)

Osvjetljenje je riješeno putem jednog svijetla (light0), kojem se kasnije dodjelila svojstva boje, pozicije i
reflektivnosti.

Boje su spremljene u svoju varijablu, te kasnije se po potrebi pozivaju prije kreiranja novog objekta.
(redosljed je važan)

Dodatni opisi se nalaze u samom kodu.

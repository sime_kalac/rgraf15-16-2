find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
#if(CMAKE_COMPILER_IS_GNUCXX)
#	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -Werror")
#endif()

ADD_EXECUTABLE(kruzic kruzic.cpp)
target_link_libraries(kruzic ${OPENGL_LIBRARIES} ${GLUT_LIBRARY})
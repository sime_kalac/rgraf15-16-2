#include <stdlib.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <string.h>

bool wire = false;
bool full = false;
char stanje []="Pocetak";
GLfloat cX=0.0;
GLfloat cY=0.0;

GLfloat ambientLight[]  = { 0.3, 0.3, 0.3, 1.0 };
GLfloat diffuseLight[]  = { 1.0, 1.0, 1.0, 1.0 };
GLfloat specularLight[] = { 1.0, 1.0, 1.0 };
GLfloat whiteSpecMate[] = { 1.0, 1.0, 1.0 };
GLfloat lightPosition[] = { 0.0, 5.0, 0.0, 1.0 };
GLfloat shininess[]     = { 50 };


//5 zasebnih dijelova čaše
float kontura[] = {
   -2.5, 1, 1,
 -0.5, 1, 0,
 -0.5, 7, 0,
 -2.5, 10, 0,
   -3, 15, 0,
};


float angle = 0.0f;


void light (void) {
     /* Omogucavanje svijetla */
    glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
    glLightfv(GL_LIGHT0, GL_AMBIENT,  ambientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  diffuseLight);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

    /* Udaljenost objekta utjece na njegovo osvjetljenje */
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.01);

    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);

    /* Boja kao materijal */
    glEnable(GL_COLOR_MATERIAL);

    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,  whiteSpecMate);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);

    /* Eliminacija skrivenih povrsina */
    glEnable(GL_DEPTH_TEST);

    //glShadeModel(GL_FLAT);
    glShadeModel(GL_SMOOTH);
    
}
//funkcija za ispis trenutnog stanja čaše u aplikaciji
void drawString (void * font, char *s, float x, float y, float z){
    unsigned int i;
    glColor4f(1, 0.3, 0.5, 1);
    glRasterPos3f(x, y, z);

    for (i = 0; i < strlen (s); i++) {
        glutBitmapCharacter (font, s[i]);
    }
    return;
}

void display()
{
   
    glClearColor(1.0f, 0.3, 0.3f, 1.0f);//pozadina
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -13.0f);
    
    drawString(GLUT_BITMAP_HELVETICA_18, stanje, cX-(-6.0), cY-(-8.0), 0);
    
    glRotatef(angle--, 0, 1, 0);
  

    glTranslatef(0, -7, 0);
   

    int vel = sizeof(kontura)/sizeof(*kontura);
    float a[vel];
    float b[vel];
    
    
    memcpy(a, kontura, sizeof(kontura));
    
    int br;
    if(full)br=30;
    else br = 15;
    
    //petlje za crtanje čaše(ovisno o stanju) pomoću linija(žičani model) ili pomoću trokuta i pravokutnika(puni model)
    for(int i = 0; i < br + 1; i++)
    {
      for(int j = 0; j < vel; j += 3)
      {
      b[j+1] = kontura[j+1];
      b[j] = kontura[j] * cos(360/30 * i * 3.14/180);
      b[j+2] = kontura[j] * sin(360/30 * i * 3.14/180);

      }
      if (wire)
      {
        for(int j = 0; j < vel-3; j+=3)
            {
                  glBegin(GL_LINES);
                  glColor3f(0.7, 0.7, 0.7);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(a[j+3], a[j+4], a[j+5]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glEnd();
             }
        }
        else
        {
          for(int j = 0; j < vel-3; j+=3)
              {
                  glBegin(GL_QUADS);
                  glColor4f(0.3, 0.9, 0.9, 0.9);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(a[j+3], a[j+4], a[j+5]);
                  glEnd();

                  glBegin(GL_TRIANGLES);
                  glColor4f(0.9, 0.9, 0.9, 0.9);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glEnd();
                }
          }
          memcpy(a, b, sizeof(kontura));
          }
          
          
      glutSwapBuffers();
      glutPostRedisplay();
  }

void keyboard(unsigned char key, int , int ) {

        switch (key)
        {
                case 27:
                       exit(0);
                case 's':
                        if(full)full=false;
                        else {
                        full = true;
                        sprintf(stanje, "Cijela casa");
                        }
                case 'w':
                        if(wire)wire=false;
                        else 
                        {
                        wire=true;
                        sprintf(stanje,"Zicani model");
                        }
        }
}

void reshape (int width, int height){
    glViewport (0, 0, (GLsizei)width, (GLsizei)height);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluPerspective (80, (GLfloat)width / (GLfloat)height, 1.0, 100.0);
    glMatrixMode (GL_MODELVIEW);
}

int main(int argc, char** argv)
{
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize (500, 500);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("Rotirajuća čaša");
    
    
    glutDisplayFunc (display);
    glutIdleFunc (display);
    glutKeyboardFunc (keyboard);
    glutReshapeFunc (reshape); 
    light();

    glutMainLoop ();
    return 0;
}
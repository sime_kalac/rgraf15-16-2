Projekt je izraðen u vie programa zbog kvara raèunala,no na kraju je izraðen u  QT Creatoru.     . 
 
Boja pozadine je postavljena na svijetlo bijelu.
 
Za zadatak sam imala napraviti dva klatna od kojih se svaki rotira oko vlastitih osiju x, y i z.Da bi izvrili zadatak nacrtala sam sivi pravokutnik 
na koji je objeena zelena kugla,a na koju je objeeno prvo klatno (koje je ustvari cilindar plave boje).  
Na dnu prvog klatna je objeena druga kugla,takoðer zelene boje na koju je spojeno drugo klatno (crveni cilindar).Na drugo klatno je spojena uta kugla. 

Za kretanje kroz program sam koristila iduæe tipke za navigaciju:
sa s pokreæem animaciju, sa x pauziram animaciju,dok sa ESC izlazim iz prozora animacije.

Pomoæu tipki q, w i e mjenjam rotacije za gornje klatno, dok pomoæu b, m i n mjenjam os rotacije za donje klatno. Prvo klatno tj. plavi cilindar se rotira 
oko zelene kugle (spojene na sivi pravokutnik) pomoæu tipki q, w i e. Drugo klatno (crveni cilindar) se rotira oko druge zelene kugle(spojene na plavi cilndar) 
pomoæu tipki b, m, n.

Osvjetljenja su oba cilindra i sve tri kugle kako je i traeno u zadatku.
 
Koriten je prozor OpenGL-a velièine 800x480 jer smatram da najbolje odgovara prikazu.

   
Reference:

-https://www3.ntu.edu.sg/home/ehchua/programming/opengl/CG_Introduction.html 
-http://www.glprogramming.com/red/appendixi.html 
-https://open.gl/textures
-https://www.youtube.com/watch?v=kGQCSd7yp2s
-https://www.youtube.com/watch?v=SAmD_Aq1Un4
-https://www.youtube.com/watch?v=Kujd0RTsaAQ
-https://www.youtube.com/watch?v=urIBfLHSLg0
-http://www.gamedev.net/page/resources/_/technical/opengl/lighting-and-normals-r1682
-http://www.matrix44.net/cms/notes/opengl-3d-graphics/the-ads-lighting-model
Računalna Grafika

Sandi Files - Zadatak 1

Radno okruženje:
OS: Arch Linux
Software: KDevelop 4

Kratak opis samog zadatka:
Sfera plave boje inicijalno je postavljena u gornjem lijevom kutu prozora.
Pritiskom na tipku 's' pokreće se animacija i sfera se miče po definiranim bezierovim krivuljama,
animacija se zaustavlja ili pritiskom na tipku 'x' ili kada sfera dođe u dolnji deski kut prozora.
Pritiskom na tipku 'ESC' izlazi se iz programa.

Na početku koda ispisane su sve globalne varijable.
Moguće je mijenjati poziciju kamere, prikaz linija po kojima se 
kreće sfera i broj poligona pri crtanju sfere.
Bezierove linije mogu se prikazati pomocu dvije boolove varijable na na kraju ispisa globalnih varijabli.

Sam kod je detaljno komentiran radi lakšeg čitanja.

Par referentnih stranica korištenih za dovršetak zadaće
http://www.opengl-tutorial.org
http://www.stackoverflow.com
http://www.youtube.com
http://www.glprogramming.com

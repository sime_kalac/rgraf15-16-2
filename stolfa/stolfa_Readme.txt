Zadatak 4. - Ispaljivanje sfere po bezierovoj krivulji

Program je napisan u C++ programskom jeziku, te se compilira uz pomoc cmake-a.

Kontrole:
Tipka "s" -> pokretanje animacije
Tipka "e" -> zaustavljanje animacije
Tipka "Esc" -> izlaz iz programa
Strelice lijevo, desno, gore i dolje -> promjena smjera i oblika putanje gdje ce se ispaliti sfera.
Strelice rade jedino dok je animacija zaustavljena, posto se oblik putanje ne moze mijenjati nakon sto je sfera ispucana.

Program inicijalno pokazuje osjencanu i osvjetljenu sferu koja je napravljena pomocu parametarske jednadzbe sfere. Ideja je generirati
vertexe uz pomoc parametarske jednadzbe sfere. Koristeno je GL_QUAD_STRIP

Animacija se dobiva uz pomoc dinamickog generiranja bezierove krivulje, to jest koristeci kontrolne tocke, te racunanjem sljedece pozicije
tokom animacije. Putanja je zamisljena kao ciljanje sa topom, to jest tocke se micu pod kutem. Nije moguce micati tocku po tocku zasebno,
vec je potrebno naciljati. Takoder, kutevi putanje, to jest pomicanje kontrolnih tocaka je ograniceno.

Reference:
http://mathworld.wolfram.com/Sphere.html
https://open.gl/
http://www.opengl-tutorial.org/ 
https://www.opengl.org/discussion_boards/showthread.php/185186-Blending
- Primjeri sa vjezbi i predavanja
A Tour of C++, Bjarne Stroustrup, 2013.
/*
Alen Salkanovic - zadatak 4.
*/

#include <GL/gl.h>
#include <GL/glut.h>

#include <vector>
#include <algorithm>
#include <iostream>
//#include "gl/glut.h"

//#ifndef uint
//#define uint unsigned int
//#endif

#define X 0.525731f
#define Z 0.850650f

float control_points[4][3] = {  //kontrolne tocke, promjena ovog mjenja oblik bezierove krivulje
	{-4,-4,0},
	{-2,4,0},
	{2,-4,0},
	{4,4,0}
};

GLfloat diffuseMaterial[] =  {0.85f, 0.5f, 0.25f}; //postavi material u crveno
GLfloat specularMaterial[] = {1.0f,  1.0f, 1.0f};  //postavi material u bijelo
GLfloat ambientMaterial[] =  {0.2f, 0.2f, 0.2f};   //postavi material u zeleno
GLfloat mShininess[]       = {128.0f};			   //postavljanje sjaja material-a

GLfloat whiteSpecularLight[] = {1.0, 1.0, 1.0}; //odredi light specular u bijelo
GLfloat whiteDiffuseLight[]  = {1.0, 1.0, 1.0}; //odredi  diffuse light u bijelo
GLfloat whiteAmbientLight[]  = {1.0, 1.0, 1.0}; //odredi ambient light u bijelo
GLfloat positionLight[]      = {0.0f, 0.0f, -5.0f, 1.0f};	// polozaj izvora svjetla ispred pozicije ocista

float currentT = 0.0f;
float tInc = 0.0f;
float rot = 0.0f;	// rotacija krivulje

void init (void) {	// postavljam opengl parametre
    glEnable (GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glShadeModel(GL_SMOOTH);
    glEnable (GL_LIGHTING);
    glEnable (GL_LIGHT0);
}

void light (void) {	// postavljanje paramtera svjetla
    glLightfv(GL_LIGHT0, GL_SPECULAR, whiteSpecularLight);
    glLightfv(GL_LIGHT0, GL_AMBIENT, whiteAmbientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDiffuseLight);
    glLightfv(GL_LIGHT0, GL_POSITION, positionLight);
}

float *midPoint(float *a, float *b, float * result) {	// nadji mi srednju tocku izmedju dvije tocke moje kugle (i normaliziraj)
	result[0] = a[0] * 0.5f + b[0] * 0.5f;				
	result[1] = a[1] * 0.5f + b[1] * 0.5f;
	result[2] = a[2] * 0.5f + b[2] * 0.5f;
	float norm = sqrt(result[0] * result[0] + result[1] * result[1] + result[2] * result[2]);
	result[0] /= norm; result[1] /= norm; result[2] /= norm;
	return result;
}

void subdivide(float *a, float *b, float *c, int depth) {  // podijeli trokut a,b,c sve dok depth=2
	if (depth==3) {
		glNormal3fv(a);
		glVertex3fv(a);
		glNormal3fv(b);
		glVertex3fv(b);
		glNormal3fv(c);
		glVertex3fv(c);
	}
	else {
		float ab[3], bc[3], ac[3];
		midPoint(a,b,ab); 
		midPoint(a,c,ac); 
		midPoint(b,c,bc); 
		depth++;
		subdivide(a,ab,ac,depth);
		subdivide(ab,b,bc,depth);
		subdivide(bc,c,ac,depth);
		subdivide(ab,bc,ac,depth);
	}
}

void drawSphere() {		// nacrtaj kuglu koristeci icosahedron subdivision
    float ico_vert[12][3] = {  //ne koristiti gluSphere
		{-X, 0.0f, Z},
		{X, 0.0f, Z},
		{-X, 0.0f, -Z},
		{X, 0.0f, -Z},
		{0.0f, Z, X},
		{0.0f, Z, -X},
		{0.0f, -Z, X},
		{0.0f, -Z, -X},
		{Z, X, 0.0f},
		{-Z, X, 0.0f},
		{Z, -X, 0.0f},
		{-Z, -X, 0.0f}
	};

	float ico_index[20][3] = {
		{0,4,1},
		{0,9,4},
		{9,5,4},
		{4,5,8},
		{4,8,1},
		{8,10,1},
		{8,3,10},
		{5,3,8},
		{5,2,3},
		{2,7,3},
		{7,10,3},
		{7,6,10},
		{7,11,6},
		{11,0,6},
		{0,1,6},
		{6,1,10},
		{9,0,11},
		{9,11,2},
		{9,2,5},
		{7,2,11} 	
	};

	//float mid[3];

	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,   specularMaterial);
	glMaterialfv(GL_FRONT_AND_BACK,  GL_SHININESS, mShininess);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE,    diffuseMaterial);
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT,    ambientMaterial);
	glBegin(GL_TRIANGLES);
	for (int t=0; t<20; t++) {
		int i = ico_index[t][0];
		int j = ico_index[t][1];
		int k = ico_index[t][2];
		subdivide(ico_vert[i], ico_vert[j], ico_vert[k], 0); // gornja funckija za dijelit troukut
	}
	glEnd();
}

void interpolate(float *a, float *b, float t, float *r) {	// interpolacija izmejdu dvije tocke a i b
	r[0] = (1.0f-t) * a[0] + t * b[0];
	r[1] = (1.0f-t) * a[1] + t * b[1];
	r[2] = (1.0f-t) * a[2] + t * b[2];
}

void evaluate(float t, float *p) {	// defniram Bezier krivulju koristeci casteljau algoritam za4 kontrolne tocke
	float m01[3], m12[3], m23[3], m012[3], m123[3];
	interpolate(control_points[0], control_points[1], t, m01);
	interpolate(control_points[1], control_points[2], t, m12);
	interpolate(control_points[2], control_points[3], t, m23);
	interpolate(m01, m12,   t, m012);
	interpolate(m12, m23,   t, m123);
	interpolate(m012, m123, t, p);
}

void drawBezier() {	// ovdje crtam bezierovu krivulju
	glColor3f(1,1,0);
	glDisable(GL_LIGHTING);
	glBegin(GL_LINE_STRIP);
	for (float t=0.0f; t<=1.001f; t+=0.025f)	{
		float p[3];
		evaluate(t, p);
		glVertex3fv(p);
	}
	glEnd();
	glEnable(GL_LIGHTING);
}

void drawScene() {	// funkcija koja crta scenu: bezier krivulju i prije nacrtanu kuglu
	glTranslatef(0,0,-8);
	glRotatef(rot, 0,0,1);	// rotacija oko z-osi
	drawBezier();
	float p[3];
	evaluate(currentT, p);
	glTranslatef(p[0],p[1],p[2]);
	drawSphere();
	currentT += tInc;
	if (currentT > 1.0f) currentT = 1.0f;
}

void display (void) {	// poziva se za renderiranje sljedeceg frame-a
    glClearColor (0.0,0.0,0.0,1.0);
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    light();
    drawScene();
    glutSwapBuffers();
}

void reshape (int w, int h) {	//poziva se prilikom resize-anja prozora
    glViewport (0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluPerspective (60, (GLfloat)w / (GLfloat)h, 1.0, 100.0);
    glMatrixMode (GL_MODELVIEW);
}

void keyboard (unsigned char key, int , int ) {	// tu se provjerava koja tipka je 
	if (key=='s') 									// pretisnuta na tipkovnici
		tInc = 0.0001f;
	else if (key=='e') 
		tInc = 0.0f;
	else if (key==27) 
		exit(1);
}

void scaleCurve(float s) {
	for (int i=0; i<4; i++)
		control_points[i][1] *= s;
}

void specialKeys( int key, int , int  ) { // za provjeru strelica
	if (key==100)	// lijevi
		rot += 1;
	else if (key==102)	// desno
		rot -= 1;
	else if (key==101)	// gore
		scaleCurve(1.1f);
	else if (key==103)  // dolje
	
		scaleCurve(1.0/1.1f);
}

int main (int argc, char **argv) {	//main function
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize (500, 500);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("Bezier and Sphere");
    init();
    glutDisplayFunc (display);
    glutIdleFunc (display);
    glutKeyboardFunc (keyboard);
	glutSpecialFunc(specialKeys);  
    glutReshapeFunc (reshape);
    glutMainLoop ();
    return 0;
}
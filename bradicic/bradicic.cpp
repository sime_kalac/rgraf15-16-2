#include <GL/glut.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <iostream>
#include <vector>

#include <iostream>
#include <Eigen/Dense>
using namespace Eigen;
using namespace std;

/**********************
 * GLOBALNE VARIJABLE *
 **********************/
 
//UKLJUCITE OVO AKO ZELITE VIDJETI LINIJE PO KOJIMA SE LOPTA KRECE
bool dcp=false; //kontrolne točke
bool dbc=false; //bezierova linija točke
// keyevi koji se koriste
unsigned char start_key   = 's';
unsigned char stop_key    = 'x';
unsigned int  exit_key     = 27;
//pozicija kamere
float camera_x=5.0, camera_y=1.0, camera_z=9.0;
// animacija inicijalno postavljena da stoji
bool animation = false;
// postavke za bezierovu liniju
Vector4d pt_x, pt_y, pt_z;
VectorXd t_x, t_y, t_z;
VectorXd ts;
//postavke sfere
float sphereRadius=0.5;
float brojPoligona=16;
//kut okretanja
float angle=0;
float angleDiff=0;
//postavke osvjetljenja
GLfloat ambient[]  = { 0.2, 0.2, 0.2, 1.0};
GLfloat diffuse[]  = { 1, 1, 1, 1.0};
GLfloat specular[] = { 1.0, 1.0, 1.0, 1.0};
GLfloat lightPos[] = { 5.0, 3.0, 0.0, 1.0 }; //pozicija svijetla
GLfloat shine[] = { 90 };
//postavke za animiranje
float step=0;
float dist_change=0;
float dist_reset=0;
float dist=0;
// vektori za crtanje putanje
vector<float> x_path;
vector<float> y_path;

long binomials ( long n, long k )
{
    // n && k >=0
    long i;
    long b;

    if ( 0 == k || n == k )
    {
        return 1;
    }
    if ( k > n )
    {
        return 0;
    }
    if ( k > ( n - k ) )
    {
        k = n - k;
    }
    if ( 1 == k )
    {
        return n;
    }
    b = 1;
    for ( i = 1; i <= k; ++i )
    {
        b *= ( n - ( k - i ) );
        if ( b < 0 ) return -1; /* ERR!!!! OVERFLOW */
        b /= i;
    }

    return b;
}

double polyterm ( const int &n, const int &k, const double &t )
{
    return pow ( ( 1.-t ),n-k ) *pow ( t,k );
}

double getValue (float t, const VectorXd &v )
{
    int order = v.size()-1;
    double value = 0;

    if(t>1)
    {
      t=0.;
    }
    
    for ( int n=order, k=0; k<=n; k++ )
    {
        if ( v ( k ) ==0 ) continue;

        value += binomials ( n,k ) * polyterm ( n,k,t ) * v ( k );
    }

    return value;
}

void changeSize ( int w, int h )
{
    if ( h == 0 )
    {
        h = 1;
    }

    float ratio = 1.0* w / h;

    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    glViewport ( 0, 0, w, h );
    gluPerspective ( 45,ratio,1,1000 ); // view angle u y, aspect, near, far
}

void drawControlPoints()
{
    glPushMatrix();
      glColor3f ( 1.0f, 1.0f, 1.0f );
      glBegin ( GL_LINE_STRIP );
	glVertex3f ( pt_x ( 0 ), pt_y ( 0 ), pt_z ( 0 ) );
	glVertex3f ( pt_x ( 1 ), pt_y ( 1 ), pt_z ( 1 ) );
	glVertex3f ( pt_x ( 2 ), pt_y ( 2 ), pt_z ( 2 ) );
	glVertex3f ( pt_x ( 3 ), pt_y ( 3 ), pt_z ( 3 ) );
      glEnd();
    glPopMatrix();
}

void generateBezierCurve()
{
    ts = VectorXd::LinSpaced ( 21,0,1. );
    t_x.resize ( ts.size() );
    t_y.resize ( ts.size() );
    t_z.resize ( ts.size() );

    for ( int idx=0; idx< ts.size(); ++idx )
    {
        t_x ( idx ) = getValue ( ts ( idx ), pt_x );
        t_y ( idx ) = getValue ( ts ( idx ), pt_y );
        t_z ( idx ) = getValue ( ts ( idx ), pt_z );
    }
}

void drawBezierCurve()
{
    glPushMatrix();
    glColor3f ( 1.0f, 0.0f, 0.0f );
    glBegin ( GL_LINE_STRIP );
    for ( int i=0; i< ts.size(); ++i )
    { 
      glVertex3f ( t_x ( i ), t_y ( i ), t_z ( i ) ); 
    }
    glEnd();
    glPopMatrix();
}

void createSphere(double r, int lats, int longs) {
    int i, j;
    for(i = 0; i <= lats; i++) {
       double lat0 = M_PI * (-0.5 + (double) (i - 1) / lats);
       double z0  = sin(lat0);
       double zr0 =  cos(lat0);

       double lat1 = M_PI * (-0.5 + (double) i / lats);
       double z1 = sin(lat1);
       double zr1 = cos(lat1);

       glBegin(GL_QUAD_STRIP);
       glColor4f(0, 0, 1, 1);
       for(j = 0; j <= longs; j++) {
           double lng = 2 * M_PI * (double) (j - 1) / longs;
           double x = cos(lng);
           double y = sin(lng);
	    
           glNormal3f(r*x * zr0, r*y * zr0, r*z0);
           glVertex3f(r*x * zr0, r*y * zr0, r*z0);
           glNormal3f(r*x * zr1, r*y * zr1, r*z1);
           glVertex3f(r*x * zr1, r*y * zr1, r*z1);
       }
       glEnd();
   }
 }

void drawSphere()
{
  
    float x,y,z;
    if(angle!=358)angle+=angleDiff;else angle=0;
    x = getValue(dist, pt_x);
    y = getValue(dist, pt_y);
    z = getValue(dist, pt_z);
    glPushMatrix();  
    glTranslatef(x,y,z);   
    glRotatef(angle, 0, 0, -1);
    createSphere(sphereRadius, brojPoligona,brojPoligona);
    glPopMatrix();
    
}

void drawPlane()
{
    glPushMatrix();
      glNormal3f(0.0,1.0,0.0); //normala kvadrata
      glColor3f(0.4, 0.0, 0.0); //boja kvadrata
	glBegin(GL_QUAD_STRIP); //kreiranje kvadrata 
	  glVertex3f(2, -sphereRadius, -2);
	  glVertex3f(2, -sphereRadius, 2);
	  glVertex3f(10, -sphereRadius, -2);
	  glVertex3f(10, -sphereRadius, 2);
	glEnd();
    glPopMatrix();
}

void enableLights() {

    //omogucavanje svijetla 
    glLightfv(GL_LIGHT0, GL_AMBIENT,  ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.001);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);

    // Boja kao materijal 
    glEnable(GL_COLOR_MATERIAL);
    
    glMaterialfv(GL_FRONT, GL_SHININESS, shine);
    
    //glShadeModel(GL_FLAT);
    glShadeModel(GL_SMOOTH);

}

void createPath() {
    
  
    float x = getValue(dist, pt_x);
    float y = getValue(dist, pt_y);
    x_path.push_back (x);
    y_path.push_back (y);
    
    // iscrtavanje putanje
    glBegin(GL_QUAD_STRIP);
    for (unsigned int i = 0; i < x_path.size(); i++) {
	glColor4f(0.4, 0, 0, 0);
	glNormal3f(x_path[i], y_path[i], 0);
	glVertex3f(x_path[i]+0.03f, y_path[i]+0.03f, 0);
	glVertex3f(x_path[i]+0.03f, y_path[i]-0.03f, 0);
	glVertex3f(x_path[i]-0.03f, y_path[i]+0.03f, 0);
	glVertex3f(x_path[i]-0.03f, y_path[i]-0.03f, 0); 
	glVertex3f(x_path[i]+0.03f, y_path[i]+0.03f, 0); 
    }
    glEnd();
} 
void drawScene()
{
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glClearColor (0.0, 0.0, 0.0, 0.0);
    glMatrixMode ( GL_MODELVIEW );
    glLoadIdentity();
    gluLookAt ( camera_x,camera_y,camera_z, //kooridante kamere
                5.0,2.0,0.0, //kamo gleda kamera
                0.0f,1.0f,0.0f ); //up vector
     //omogucavanje svijetla 
    glPushMatrix();  
      drawPlane();

      glPushMatrix();
	  createPath();
	glPushMatrix();
	  glColor3f(0.0, 0.0, 1.0); //sphere color
	  drawSphere();
	  enableLights();
	glPopMatrix();
	if(dcp) drawControlPoints(); //crtanje kontrolnih točaka
	if(dbc) drawBezierCurve(); //crtanje bezierove krivulje
      glPopMatrix();
    glPopMatrix();  
    glutSwapBuffers();
}

void update ( int /*value*/ )
{
    glutPostRedisplay();
    if(animation) 
    {
      if(dist_reset==504)
      {
	step=0; angleDiff=0;
      }else 
      {
	step=1; angleDiff=2;
      }
    } 
    else
    {
      step=0; angleDiff=0;
    }
  
    dist_change+=step;
    dist_reset+=step;
    dist = dist_change/100;
    
    if (dist_reset == 101)
    {
      pt_x << 3, 3.5, 4.5, 5;
      pt_y << 0, 2.5, 2.5, 0 ;
      pt_z << 0, 0, 0, 0;
      generateBezierCurve();
      dist_change=0;
    }
    else if (dist_reset == 202)
    {
      pt_x << 5, 5.25, 6, 6.25;
      pt_y << 0, 1.5, 1.5, 0 ;
      pt_z << 0, 0, 0, 0;
      generateBezierCurve();
      dist_change=0;
    }
    else if (dist_reset == 303)
    {
      pt_x << 6.25, 6.5, 7, 7.25;
      pt_y << 0, 0.75, 0.75, 0 ;
      pt_z << 0, 0, 0, 0;
      generateBezierCurve();
      dist_change=0;
    }
    else if (dist_reset == 404)
    {
      pt_x << 7.25, 7.5, 7.75, 8;
      pt_y << 0, 0.4, 0.4, 0 ;
      pt_z << 0, 0, 0, 0;
      generateBezierCurve();
      dist_change=0;
    }
    else if (dist_reset == 504)
    {
      animation = false;
    }
    
    glutTimerFunc ( 20, update, 0 ); //update glut-a nakon 20 ms
}

// metoda za upravljanje
void input_key(unsigned char key, int, int) 
{
    if (tolower(key) == start_key) {
        animation = true;
    }
    else if (tolower(key) == stop_key) {
        animation = false;
    }
    else if (key == exit_key) {
        exit(0);
    }
}

int main ( int argc, char **argv )
{
    //prva bezierova krivulja
    pt_x << 1, 1.5, 2.5, 3;
    pt_y << 3, 3, 2, 0 ;
    pt_z << 0, 0, 0, 0;
    dist = 0.;
    generateBezierCurve();
    glutInit ( &argc, argv );
    
    glutInitDisplayMode ( GLUT_DOUBLE );
    glutInitWindowSize ( 1000, 1000 );
 ;
    glutCreateWindow ( "Antonio Bradicic - zadaca" );
    glutReshapeFunc ( changeSize );
    glutKeyboardFunc(input_key);
    glutDisplayFunc ( drawScene );
    glutTimerFunc ( 20, update, 0 );
    
    glutMainLoop();

    return 0;
}

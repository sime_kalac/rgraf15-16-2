#include <stdlib.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <string.h>


bool stanje = false;
float angle = 0.0f;

//osvjetljenje
GLfloat ambient[]  = { 0.4, 0.4, 0.4, 1.0 }; 
GLfloat diffuse[]  = { 1.0, 1.0, 1.0, 1.0 };
GLfloat spec[] = { 1.0, 1.0, 1.0 };
GLfloat spec_mat[] = { 1.0, 1.0, 1.0 };
GLfloat shinn[]     = { 128 };



// kontura case
float casa[] = {
	-2.5, 1, 1,
 	-0.5, 1, 0,
 	-0.5, 5.2, 0,
	-1.3, 6.2, 0,
	-1.78, 7.5, 0,
 	-2.3, 10, 0,
	-2.55, 12.5, 0,
	-2.45, 15, 0,
};



void light (void) { //standardne komande za osvjetljenje
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glLightfv(GL_LIGHT0, GL_SPECULAR, spec);
	glLightfv(GL_LIGHT0, GL_AMBIENT,  ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE,  diffuse);


	glEnable(GL_COLOR_MATERIAL);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,  spec_mat);
    	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shinn);  

}

void display()
{
   
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
 
    
    glTranslatef(0.0f, -9, -13.0f);
    glRotatef(angle--, 0, 1, 0);

	    
    glEnable(GL_DEPTH_TEST); 
    glShadeModel(GL_SMOOTH); //omogucuje gladi izgled povrsine


    int vel = sizeof(casa)/sizeof(*casa);
    float a[vel];
    float b[vel];

    memcpy(a, casa, sizeof(casa));
    int br=120; 
    //algoritam za crtanje modela
    for(int i = 0; i < br + 1; i++)
    {
      for(int j = 0; j < vel; j += 3)
      {
      b[j+1] = casa[j+1];
      b[j] = casa[j] * cos(720/60 * i * 3.14/180);
      b[j+2] = casa[j] * sin(720/60 * i * 3.14/180);

      }
      if (stanje)
      {
        for(int j = 0; j < vel-3; j+=3)
            {
		  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                  glBegin(GL_TRIANGLE_STRIP);
                  glColor4f(0.5, 0.5, 0.5, 0.3);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(a[j+3], a[j+4], a[j+5]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glEnd();
             }
        }
        else
        {
          for(int j = 0; j < vel-3; j+=3)
              {
        
                  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
                  glBegin(GL_TRIANGLE_STRIP);
                  glColor4f(0.5, 0.5, 0.5, 0.3);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(a[j+3], a[j+4], a[j+5]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glEnd();
                }
          }
          memcpy(a, b, sizeof(casa));
          }

          
      glutSwapBuffers();   
      glutPostRedisplay();
  }

void keyboard(unsigned char key, int , int ) {

        switch (key)
        {
                case 27:
                       exit(0);
               
                 case 's':
                      if(stanje)stanje=false;

                      	break;
               case 'w':
                       if(!stanje)stanje=true;
			break;
      }

        
}

void reshape (int width, int height){
    glViewport (0, 0, (GLsizei)width, (GLsizei)height);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluPerspective (80, (GLfloat)width / (GLfloat)height, 1.0, 80.0);
    glMatrixMode (GL_MODELVIEW);
}

int main(int argc, char** argv)
{
	//inicijalizacija prozora
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize (600, 600);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("Edo Kanizic Staklena casa");

    // za prozirnost materijala
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
    glEnable(GL_BLEND);

    
    glutDisplayFunc (display);
    glutIdleFunc (display);
    glutKeyboardFunc (keyboard);
    glutReshapeFunc (reshape); 
    light();

    glutMainLoop ();
    return 0;
}
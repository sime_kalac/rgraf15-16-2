Program je pisan u radnom okruzenju KDevelop. Unutar koda se nalaze komentari za odredene linije koda.
Program je kompajliran preko terminala ubuntu-a bez dodatnih argumenata. 

Komande za upravljanje programom: 
	-ESC: izlazak iz programa
	-W: zicani prikaz modela case
	-S: prikaz cijelog modela case
	
Od dodatnog sadrzaja, pored kodova s vjezbi koristene su sljedece stranice:

http://www.lousodrome.net/opengl/
https://web.math.pmf.unizg.hr/nastava/CG/Pred_1.html
https://www.opengl.org/wiki/
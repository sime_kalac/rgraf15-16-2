Opis
﻿Zadaća iz računalne grafike, zadatak broj 2. - staklena čaša  

Upute za pokretanje:

- Pritiskom na tipku 'w' prikaže se žičani model plohe
- Pritiskom na tipku 's' prikaže se cijeli model
- Pritiskom na tipku 'Esc' izlazimo iz programa
 

Razvojna okolina:

- KDevelop 4 na Linux Mint
- GLUT i GL paket


Prilikom pokretanja animacije otvara se prozor veličine 800x800 unutar kojeg je vidljiva čaša koja se rotira oko središnje osi od 90 stupnjava (izabrano proizvoljno).  Pri pokretanju nije potrebno unositi nikakve argumente. Vidljiva je cijela čaša za vrijeme animacije. 
Pritiskom na tipku 'w'  prikazuje se vanjska kontura čaše, a ponovnim pritiskom prikaže se žičani model plohe. Pritiskom na tipku 's' prikazuje se vanjska kontura, a ponovnim pritiskom prikaže se cijeli model. Animacija završava pritiskom na tipku 'Esc'.

Materijal je proziran, uključeno je osvjetljenje I čaša je osjenčana.


Kao reference koristila sam:

-primjere sa vježbi, a ponajviše vježbu br.7. 
-primjere sa OpenGL wiki te dokumentaciju na www.opengl.org.
-http://docslide.us/documents/3d-rendering.html...